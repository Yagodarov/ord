<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class InitDatabaseWorkbench extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertisers', function (Blueprint $table) {
           $table->id();
           $table->bigInteger('inn')->nullable();
           $table->string('name', 64)->nullable();
           $table->timestamps();
        });
        Schema::create('agencies', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->bigInteger('inn')->nullable();
            $table->timestamps();
        });
        Schema::create('contracts', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('amount')->nullable();
            $table->dateTime('date')->nullable();
            $table->string('documentationLink', 256)->nullable();
            $table->boolean('isVat')->default(false);
            $table->integer('number')->nullable();
            $table->bigInteger('externalId');
            $table->bigInteger('externalContractorId');
            $table->bigInteger('externalClientId');
            $table->unique(['externalId','externalContractorId','externalClientId'], 'id_contr_client_unique');
            $table->foreignId ('agency_id')->constrained();
            $table->timestamps();
        });
        Schema::create('campaigns', function (Blueprint $table) {
            $table->id();
            $table->dateTime('startDate')->nullable();
            $table->dateTime('endDate')->nullable();
            $table->bigInteger('externalId')->unique();
            $table->foreignId ('advertiser_id')->constrained();
            $table->timestamps();
        });
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('amount')->nullable();
            $table->string('documentLink', 256)->nullable();
            $table->dateTime('endDate')->nullable();
            $table->boolean('isVat')->nullable();
            $table->dateTime('startDate')->nullable();
            $table->bigInteger('externalId')->unique();
            $table->foreignId ('campaign_id')->constrained();
            $table->foreignId ('contract_id')->constrained();
            $table->timestamps();
        });
        Schema::create('targeting_regions', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });
        Schema::create('creatives', function (Blueprint $table) {
            $table->id();
            $table->integer('category')->nullable();
            $table->integer('type')->nullable();
            $table->string('url', 256)->nullable();
            $table->string('mediaUrl', 256)->nullable();
            $table->text('textData')->nullable();
            $table->bigInteger('externalId')->unique();
            $table->foreignId ('targeting_region_id')->constrained();
            $table->foreignId ('campaign_id')->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
