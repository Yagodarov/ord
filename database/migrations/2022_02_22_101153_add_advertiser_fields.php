<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdvertiserFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('advertisers', function (Blueprint $table) {
            $table->string('citizenName', 256)->nullable();
            $table->bigInteger('numberPay')->nullable();
            $table->bigInteger('codeCountry')->nullable();
            $table->bigInteger('regNumberFor')->nullable();
            $table->bigInteger('innFor')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('advertisers', function (Blueprint $table) {
            $table->dropColumn('citizenName');
            $table->dropColumn('numberPay');
            $table->dropColumn('codeCountry');
            $table->dropColumn('regNumberFor');
            $table->dropColumn('innFor');
        });
    }
}
