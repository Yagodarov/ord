<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAgencyFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('agencies', function (Blueprint $table) {
            $table->bigInteger('ogrn')->nullable();
            $table->bigInteger('numberPay')->nullable();
            $table->bigInteger('codeCountry')->nullable();
            $table->bigInteger('regNumberFor')->nullable();
            $table->bigInteger('innFor')->nullable();
            $table->string('address', 256)->nullable();
            $table->string('citizenName', 256)->nullable();
            $table->string('numberPhone', 256)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agencies', function (Blueprint $table) {

        });
    }
}
