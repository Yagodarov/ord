<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCreativeFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('creatives', function (Blueprint $table) {
           $table->dropColumn('type');
           $table->string('descriptionObject', 256)->nullable();
           $table->string('domainSource', 256)->nullable();
           $table->string('urlSource', 256)->nullable();
           $table->string('systemAdv', 256)->nullable();
           $table->integer('sex')->nullable();
           $table->integer('age')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('creatives', function (Blueprint $table) {
            $table->integer('type')->nullable();
            $table->dropColumn(['descriptionObject', 'domainSource', 'urlSource', 'systemAdv', 'sex', 'age']);
        });
    }
}
