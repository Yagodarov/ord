<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use function array_merge;

class CreateUserRoles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $manage_agencies = [
            'view_agencies',
            'create_agencies',
            'edit_agencies',
            'delete_agencies'
        ];
        $manage_campaigns = [
            'view_campaigns',
            'create_campaigns',
            'edit_campaigns',
            'delete_campaigns'
        ];
        $manage_advertisers = [
            'view_advertisers',
            'create_advertisers',
            'edit_advertisers',
            'delete_advertisers'
        ];
        $manage_contracts = [
            'view_contracts',
            'create_contracts',
            'edit_contracts',
            'delete_contracts'
        ];
        $manage_creatives = [
            'view_creatives',
            'create_creatives',
            'edit_creatives',
            'delete_creatives'
        ];
        $manage_invoices = [
            'view_invoices',
            'create_invoices',
            'edit_invoices',
            'delete_invoices'
        ];
        $manage_stats = [
            'view_stats',
            'create_stats',
            'edit_stats',
            'delete_stats'
        ];
        $all_permissions = array_merge($manage_agencies,$manage_campaigns,$manage_advertisers,$manage_contracts,$manage_creatives,$manage_invoices,$manage_stats);
        $roles = [
            'admin' =>
                $all_permissions,
            'agency' => array_merge([
                'view_agencies',
                'edit_agencies',
                'delete_agencies'],
                $manage_campaigns,$manage_advertisers,$manage_contracts,$manage_creatives,$manage_invoices,$manage_stats),
            'aggregator' => $all_permissions
        ];
        foreach ($roles as $role => $permissions)
        {
            var_dump($role);
            $role = Role::findOrCreate($role,'api');
            var_dump($role);
            foreach ($permissions as $permission) {
                $permissionCreated = Permission::findOrCreate($permission,'api');
                $role->givePermissionTo($permissionCreated);
            }
        }
    }
}
