<?php

namespace Database\Seeders;

use App\Models\Agency;
use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class TestUserCreateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $agency = User::firstOrCreate([
            'name' => 'agency',
            'email' => 'agency@email.com',
        ]);
        $agency->setAttribute('password', bcrypt('agency'));
        $agency->save();
        $agency->assignRole('agency');

        $userAgency = Agency::firstOrCreate([
            'user_id' => $agency->getAuthIdentifier()
        ]);
        $userAgency->setAttribute('inn', null);
        $userAgency->setAttribute('name', null);
        $userAgency->save();

        $aggregator = User::firstOrNew([
            'name' => 'aggregator',
            'email' => 'aggregator@email.com',
            ]);
        $aggregator->setAttribute('password', bcrypt('aggregator'));
        $aggregator->save();
        $aggregator->assignRole('aggregator');
    }
}
