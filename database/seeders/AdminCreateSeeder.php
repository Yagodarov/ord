<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class AdminCreateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User();
        $admin->setAttribute('name', 'admin');
        $admin->setAttribute('email', 'admin2@email.com');
        $admin->setAttribute('password', bcrypt('admin123'));
        $admin->save();
        $admin->assignRole('admin');
    }
}
