#!/bin/bash
set -e

if ! [ -d "./vendor" ]; then
    composer install --optimize-autoloader
#    chmod -R a+w ./vendor
#    chmod -R a+w ./storage
#    chown -R 1000:1000 ../html/
    php artisan storage:link
    php artisan migrate
    php artisan key:generate
    php artisan passport:install
#    php artisan breeze:install
    chown -R 1000:1000 .
    php artisan db:seed --class=CreateUserRoles
    php artisan db:seed --class=TestUserCreateSeeder
    php artisan db:seed --class=AdminCreateSeeder
#    php artisan command:CreateUser
#    php artisan user:create test@test.com zAq3h2M1eWq
fi

exec "$@"
