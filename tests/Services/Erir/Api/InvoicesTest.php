<?php


namespace Tests\Services\Erir\Api;


use App\Services\Erir\Api\Invoices;

class InvoicesTest extends TestCase
{
    public function testShouldCreateInvoice(): void
    {
        $api = $this->getApiMock();
        $api->expects($this->once())
            ->method('create')
            ->with(
                '200', '103', 100, 'https://example.example/invoice.pdf',
                '2022-04-25T07:23:35.858Z', '2022-04-25T07:23:35.858Z',
                [
                    [
                        'amount' =>  50,
                        'endDate' =>  '2022-04-25T07:23:35.858Z',
                        'startDate' =>  '2022-04-25T07:23:35.858Z',
                        'externalCampaignId' =>  '142'
                    ]
                ],
                true
            )
            ->will($this->returnValue(true));

        $this->assertTrue($api->create(
            '200', '103', 100, 'https://example.example/invoice.pdf',
            '2022-04-25T07:23:35.858Z', '2022-04-25T07:23:35.858Z',
            [
                [
                    'amount' =>  50,
                    'endDate' =>  '2022-04-25T07:23:35.858Z',
                    'startDate' =>  '2022-04-25T07:23:35.858Z',
                    'externalCampaignId' =>  '142'
                ]
            ],
            true
        ));
    }

    public function testShouldGetInvoice(): void
    {
        $expectedArray = [
            'externalId' => '200',
            'externalContractId' => '103',
            'amount' =>  55,
            'documentLink' =>  'https://example.example',
            'startDate' =>  '2022-04-25T07:23:35.858Z',
            'endDate' =>  '2022-04-25T07:23:35.858Z',
            'invoiceItems' =>  [
                [
                    'amount' =>  50,
                    'endDate' =>  '2022-04-25T07:23:35.858Z',
                    'startDate' =>  '2022-04-25T07:23:35.858Z',
                    'externalCampaignId' =>  '142'
                ]
              ],
            'isVat' =>  false
        ];

        $api = $this->getApiMock();
        $api->expects($this->once())
            ->method('getById')
            ->with('200')
            ->will($this->returnValue($expectedArray))
        ;

        $this->assertEquals($expectedArray, $api->getById('200'));
    }

    protected function getApiClass(): string
    {
        return Invoices::class;
    }
}