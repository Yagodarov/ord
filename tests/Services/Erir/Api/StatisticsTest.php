<?php


namespace Tests\Services\Erir\Api;


use App\Services\Erir\Api\Statistics;

class StatisticsTest extends TestCase
{
    public function testShouldCreateStatistics(): void
    {

        $api = $this->getApiMock();
        $api->expects($this->once())
            ->method('create')
            ->with(
                '142', '102', '2022-04-25T07:03:18.464Z',  '2022-04-25T07:03:18.464Z',
                [[
                      'clicks' => 20,
                      'convs' => 10,
                      'imps' => 5
                ]]
            )
            ->will($this->returnValue(true));

        $this->assertTrue($api->create(
            '142', '102', '2022-04-25T07:03:18.464Z',  '2022-04-25T07:03:18.464Z',
            [['clicks' => 20, 'convs' => 10, 'imps' => 5]]
        ));
    }

    protected function getApiClass(): string
    {
        return Statistics::class;
    }
}