<?php

declare(strict_types=1);

namespace Tests\Services\Erir\Api;

use App\Services\Erir\ErirClient;
use PHPUnit\Framework\TestCase as BaseTestCase;
use Psr\Http\Client\ClientInterface;

abstract class TestCase extends BaseTestCase
{

    abstract protected function getApiClass(): string;

    protected function getApiMock(array $methods = []): \PHPUnit\Framework\MockObject\MockObject
    {
        $httpClient = $this->getMockBuilder(ClientInterface::class)
            ->onlyMethods(['sendRequest'])
            ->getMock();
        $httpClient
            ->expects($this->any())
            ->method('sendRequest');

        $client = ErirClient::createWithHttpClient($httpClient);

        return $this->getMockBuilder($this->getApiClass())
            ->onlyMethods(\array_merge(['getById', 'postCreate', 'create', 'get', 'post'], $methods))
            ->setConstructorArgs([$client, null])
            ->getMock();
    }
}
