<?php


namespace Tests\Services\Erir\Api;


use App\Services\Erir\Api\Contracts;

class ContractsTest extends TestCase
{
    public function testShouldCreateContract(): void
    {
        $api = $this->getApiMock();
        $api->expects($this->once())
            ->method('create')
            ->with( '106', '143', '142', 10000, '2022-04-25T07:16:09.434Z',
                    'https://example.example/example.pdf', '95', true)
            ->will($this->returnValue(true));

        $this->assertTrue($api->create(
            '106', '143', '142', 10000, '2022-04-25T07:16:09.434Z', 'https://example.example/example.pdf', '95', true
        ));
    }

    public function testShouldGetContract(): void
    {
        $expectedArray = [
            'amount' => 100,
            'date' => '2022-04-25T07:16:09.434Z',
            'documentLink' => 'https://example.example/example.pdf',
            'externalClientId' => '142',
            'externalContractorId' => '143',
            'externalId' => '106',
            'isVat' => true,
            'number' => '95'
        ];

        $api = $this->getApiMock();
        $api->expects($this->once())
            ->method('getById')
            ->with('106')
            ->will($this->returnValue($expectedArray))
        ;

        $this->assertEquals($expectedArray, $api->getById('106'));
    }

    protected function getApiClass(): string
    {
        return Contracts::class;
    }
}