<?php


namespace Tests\Services\Erir\Api;


use App\Services\Erir\Api\Campaigns;

class CampaignsTest extends TestCase
{
    public function testShouldCreateCampaign(): void
    {
        $api = $this->getApiMock();
        $api->expects($this->once())
            ->method('create')
            ->with( '142',  '142',  '2022-04-25T07:03:18.464Z', '2022-04-25T07:03:18.464Z')
            ->will($this->returnValue(true));

        $this->assertTrue($api->create('142',  '142',  '2022-04-25T07:03:18.464Z', '2022-04-25T07:03:18.464Z'));
    }

    public function testShouldGetCampaign(): void
    {
        $expectedArray = [
            'externalId' => '142',
            'externalAdvertiserId' => '142',
            'startDate' => '2022-04-25T07:03:18.464Z',
            'endDate' => '2022-04-25T07:03:18.464Z'
        ];

        $api = $this->getApiMock();
        $api->expects($this->once())
            ->method('getById')
            ->with('142')
            ->will($this->returnValue($expectedArray))
        ;

        $this->assertEquals($expectedArray, $api->getById('142'));
    }

    protected function getApiClass(): string
    {
        return Campaigns::class;
    }
}