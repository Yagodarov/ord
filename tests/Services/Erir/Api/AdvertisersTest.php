<?php

namespace Tests\Services\Erir\Api;

use App\Services\Erir\Api\Advertisers;

class AdvertisersTest extends TestCase
{

    public function testShouldCreateAdvertiser(): void
    {
        $api = $this->getApiMock();
        $api->expects($this->once())
            ->method('create')
            ->with( '144',  '111111111111',  'Тестовый advertiser')
            ->will($this->returnValue(true));

        $this->assertTrue($api->create('144', '111111111111', 'Тестовый advertiser'));
    }

    public function testShouldGetAdvertiser(): void
    {
        $expectedArray = ['externalId' => '144', 'inn' => '111111111111', 'name' => 'Тестовый advertiser'];

        $api = $this->getApiMock();
        $api->expects($this->once())
            ->method('getById')
            ->with('144')
            ->will($this->returnValue($expectedArray))
        ;

        $this->assertEquals($expectedArray, $api->getById('144'));
    }

    protected function getApiClass(): string
    {
        return Advertisers::class;
    }
}
