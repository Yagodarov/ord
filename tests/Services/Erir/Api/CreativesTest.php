<?php


namespace Tests\Services\Erir\Api;


use App\Services\Erir\Api\Creatives;

class CreativesTest extends TestCase
{
    public function testShouldCreateCreative(): void
    {
        $api = $this->getApiMock();
        $api->expects($this->once())
            ->method('create')
            ->with(
                'testcategory',
                ['https://example.example/example.png'],
                ['testregion'],
                ['testtext'],
                '142',
                '102',
                'testtype',
                'https://example.example/'
            )
            ->will($this->returnValue(true));

        $this->assertTrue($api->create(
            'testcategory', ['https://example.example/example.png'], ['testregion'],
            ['testtext'], '142', '102', 'testtype', 'https://example.example/'
        ));
    }

    public function testShouldGetCreative(): void
    {
        $expectedArray = [
            'externalId' => '102',
            'externalCampaignId' =>  '142',
            'category' =>  'testcategory',
            'type' =>  'testtype',
            'url' =>  'https://example.example',
            'creativeData' =>  [
                'mediaUrl' =>  ['https://example.example/example.png'],
                'targetingRegion' =>  ['testregion'],
                'textData' =>  ['testtext']
            ]
        ];

        $api = $this->getApiMock();
        $api->expects($this->once())
            ->method('getById')
            ->with('102')
            ->will($this->returnValue($expectedArray))
        ;

        $this->assertEquals($expectedArray, $api->getById('102'));
    }

    protected function getApiClass(): string
    {
        return Creatives::class;
    }
}