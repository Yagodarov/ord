<?php

declare(strict_types=1);

return [

    /*
    * Подключение к API ЕРИР по умолчанию
    */

    'default' => 'main',

    /*
    *  Настройки подключений к API ЕРИР
    */

    'connections' => [

        'main' => [
            'token'   => env('ERIR_TOKEN'),
            'method'  => 'token',
            'cache'   => false,
            'base_url'=> 'https://dev.erir.grfc.ru/',
            'retries' => 2
        ],
    ],

    /**
     * Провайдер импорта по умолчанию
     */

    'default_import' => 'csv',

    /**
     * Провайдер маппинг данных для импорта из файлов
     */

    'import'=> [

        'csv' => [
            'delimiter' => ';',
            'has_headers' => false,
            'mapping' => [
                'advertisers' => [
                    'fields' => ['id', 'name', 'inn']
                ],
                'agencies' => [
                    'fields' => ['id', 'name', 'inn']
                ],
                'contracts' => [
                    'fields' => ['amount', 'date', 'documentLink', 'clientId', 'contractId',
                    'contractorId', 'id', 'isVat', 'number']
                ],
                'invoices' => [
                    'fields' => ['amount', 'documentLink', 'startDate', 'endDate', 'isVat', 'contractId', 'id',],
                    'detail' => 'invoice_items'
                ],
                'invoice_items' => [
                    'fields' => ['startDate', 'endDate', 'externalCampaignId', 'amount']
                ],
                'campaigns' => [
                    'fields' => ['advertiserId', 'id', 'startDate', 'endDate']
                ],
                'creatives' => [
                    'fields' => ['groupId', 'id', 'type', 'url', 'category', 'text']
                ],
                'statistics' => [
                    'fields' => ['startDate', 'endDate', 'campaignId', 'creativeId', 'clicks', 'views', 'conversions']
                ]
            ]
        ]
    ]

];
