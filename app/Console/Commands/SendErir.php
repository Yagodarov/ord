<?php

namespace App\Console\Commands;

use App\Services\Erir\Facades\Import;
use Illuminate\Console\Command;

class SendErir extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'erir:import-csv {--file=} {--entity=} {--detail=0} {--format=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Отправка данных в API ЕРИР из csv файла';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): int
    {
        $import = [
            'from' => $this->option('file'),
            'detail' => $this->option('detail')
        ];

        $entity = $this->option('entity');
        $format = $this->option('format');

        if ($format) {
            Import::provider($format)->$entity($import);
        } else {
            Import::$entity($import);
        }

        return 0;
    }
}
