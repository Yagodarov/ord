<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use const PHP_EOL;

class TestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        echo hash('md5', 'test').PHP_EOL;
        echo hash('md5', 'test').PHP_EOL;
    }
}
