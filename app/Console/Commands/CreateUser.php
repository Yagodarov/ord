<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use function bcrypt;
use function print_r;
use const PHP_EOL;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:createUser';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $user = new User();
        $user->password = bcrypt('admin123');
        $user->email = 'admin@example.com';
        $user->name = 'admin';
        $user->save();
        print_r("Successfully created user".PHP_EOL);
    }
}
