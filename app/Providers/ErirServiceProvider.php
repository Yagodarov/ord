<?php


namespace App\Providers;

use App\Services\Erir\Auth\AuthenticatorFactory;
use App\Services\Erir\ErirClient;
use App\Services\Erir\ErirFactory;
use App\Services\Erir\ErirManager;
use App\Services\Erir\Import\ErirImport;
use App\Services\Erir\Import\ImportFactory;
use App\Services\Erir\Import\Provider\ProviderFactory;
use Carbon\Laravel\ServiceProvider;
use Illuminate\Contracts\Container\Container;

class ErirServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $source = config_path('erir.php');

        if ($this->app->runningInConsole()) {
            $this->publishes([$source => config_path('erir.php')]);
        }

        $this->mergeConfigFrom($source, 'erir');
    }

    public function register(): void
    {
        $this->registerAuthFactory();
        $this->registerErirFactory();
        $this->registerErirManager();
        $this->registerProviderFactory();
        $this->registerImportFactory();
        $this->registerErirImport();
        $this->registerBindings();
    }

    public function provides(): array
    {
        return [
            'erir.authfactory',
            'erir.factory',
            'erir',
            'erir.connection',
            'erir.import'
        ];
    }

    protected function registerErirFactory(): void
    {
        $this->app->singleton('erir.factory', function (Container $app) {
            $auth = $app['erir.authfactory'];

            return new ErirFactory($auth);
        });

        $this->app->alias('erir.factory', ErirFactory::class);
    }


    protected function registerBindings(): void
    {
        $this->app->bind('erir.connection', function (Container $app) {
            $manager = $app['erir'];

            return $manager->connection();
        });

        $this->app->alias('erir.connection', ErirClient::class);
    }

    protected function registerErirManager(): void
    {
        $this->app->singleton('erir', function (Container $app) {
            $config = $app['config'];
            $factory = $app['erir.factory'];

            return new ErirManager($config, $factory);
        });

        $this->app->alias('erir', ErirManager::class);
    }

    protected function registerAuthFactory(): void
    {
        $this->app->singleton('erir.authfactory', function () {
            return new AuthenticatorFactory();
        });

        $this->app->alias('erir.authfactory', AuthenticatorFactory::class);
    }

    protected function registerProviderFactory(): void
    {
        $this->app->singleton('erir.providerfactory', function () {
            return new ProviderFactory();
        });

        $this->app->alias('erir.providerfactory', AuthenticatorFactory::class);
    }

    protected function registerImportFactory(): void
    {
        $this->app->singleton('erir.importfactory', function (Container $app) {
            $provider = $app['erir.providerfactory'];
            $api = $app['erir'];

            return new ImportFactory($provider, $api);
        });

        $this->app->alias('erir.importfactory', ErirFactory::class);
    }

    protected function registerErirImport(): void
    {
        $this->app->singleton('erir.import', function (Container $app) {
            $config = $app['config'];
            $import = $app['erir.importfactory'];

            return new ErirImport($config, $import);
        });
    }

}