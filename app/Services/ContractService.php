<?php

namespace App\Services;

use App\Http\Resources\BaseResource;
use App\Http\Resources\ContractCollection;
use App\Http\Resources\ContractResource;
use App\Models\Contract;
use App\Models\Role;
use App\Rules\CheckAdvertiserId;
use App\Rules\CheckAgencyId;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class ContractService extends BaseService
{
    public function getRules(Request $request) {
        $rules = [
            'amount' => ['sometimes','integer'],
            'date' => ['sometimes','date'],
            'isVat' => ['sometimes','string'],
            'number' => ['sometimes','integer'],
            'agency_id' => ['required','integer','exists:agencies,id', new CheckAgencyId],
            'advertiser_id' => ['required','integer','exists:advertisers,id', new CheckAdvertiserId],
            'externalId' => ['required','integer',
                Rule::unique('contracts', 'externalId')
                ->where(function ($query) use ($request) {
                    return
                        $query
                            ->where('user_id', Auth::user()->getAuthIdentifier());})
//                        ->where('externalContractorId', $request->get('externalContractorId'))
//                        ->where('externalClientId', $request->get('externalClientId'))
//            })
            ],
            'externalContractorId' => ['required','integer',
//                Rule::unique('contracts', 'externalContractorId')
//                ->where(function ($query) use ($request) {
//                return
//                    $query
//                        ->where('externalId', $request->get('externalId'))
//                        ->where('externalClientId', $request->get('externalClientId'))
//                        ->where('user_id', Auth::user()->getAuthIdentifier());
//            })
            ],
            'externalClientId' => ['required','integer',
//                Rule::unique('contracts', 'externalClientId')
//                ->where(function ($query) use ($request) {
//                return
//                    $query
//                        ->where('externalId', $request->get('externalId'))
//                        ->where('externalContractorId', $request->get('externalContractorId'))
//                        ->where('user_id', Auth::user()->getAuthIdentifier());
//            })
            ],
            'subjectContract' => ['sometimes','string','max:256'],
        ];
        if ($request->method() == 'POST') {
            return $rules;
        } else if ($request->method() == 'PUT') {
            $rules['externalId'] = ['required','integer',Rule::unique('contracts', 'externalId')->ignore($request->route('id'))
                ->where(function ($query) use ($request) {
                    return
                        $query
                            ->where('user_id', Auth::user()->getAuthIdentifier());
//                            ->where('externalContractorId', $request->get('externalContractorId'))
//                            ->where('externalClientId', $request->get('externalClientId'));
                })
            ];
            $rules['externalContractorId'] = ['required','integer',
//                Rule::unique('contracts', 'externalContractorId')->ignore($request->route('id'))->where(function ($query) use ($request) {
//                return
//                    $query
//                        ->where('externalId', $request->get('externalId'))
//                        ->where('externalClientId', $request->get('externalClientId'))
//                        ->where('user_id', Auth::user()->getAuthIdentifier());
//            })
            ];
            $rules['externalClientId'] = ['required','integer',
//                Rule::unique('contracts', 'externalClientId')->ignore($request->route('id'))->where(function ($query) use ($request) {
//                return
//                    $query
//                        ->where('externalId', $request->get('externalId'))
//                        ->where('externalContractorId', $request->get('externalContractorId'))
//                        ->where('user_id', Auth::user()->getAuthIdentifier());
//            })
            ];
        }
        return $rules;
    }

    public function getResource($model) {
        return new ContractResource($model);
    }

    public function getCollection($array)
    {
        return new ContractCollection($array);
    }

    public function __construct()
    {
        $this->className = Contract::class;
    }

    public function getModel(int $id)
    {
        $query = $this->className::query();
        $query = $this->getModelByRole($query);
        $model = $query->find($id);
        return $model;
    }

    public function getListQuery(Builder $query)
    {
        return $this->getModelByRole($query);
    }

}
