<?php


namespace App\Services\Erir;


use App\Services\Erir\Auth\AuthenticatorFactory;
use App\Services\Erir\HttpClient\Builder;
use Http\Client\Common\Plugin\RetryPlugin;
use InvalidArgumentException;
use Illuminate\Support\Arr;

class ErirFactory
{
    public function __construct(
        protected AuthenticatorFactory $auth
    )
    {}

    public function make(array $config): ErirClient
    {
        $client = new ErirClient($this->getBuilder($config));

        if (!array_key_exists('method', $config)) {
            throw new InvalidArgumentException('Erir API factory requires an auth method.');
        }

        if (!array_key_exists('base_url', $config)) {
            throw new InvalidArgumentException('Erir API factory requires base url endpoint.');
        }

        $client->setUrl(Arr::get($config, 'base_url'));

        if ($config['method'] === 'none') {
            return $client;
        }

        return $this->auth->make($config['method'])->withClient($client)->authenticate($config);
    }

    protected function getBuilder(array $config): Builder
    {
        $builder = new Builder(
            new \GuzzleHttp\Client(['verify' => false])
        );

        if ($retries = Arr::get($config, 'retries')) {
            $builder->addPlugin(new RetryPlugin(['retries' => $retries === true ? 2 : $retries]));
        }

        return $builder;
    }
}