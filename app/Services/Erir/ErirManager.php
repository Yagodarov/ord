<?php


namespace App\Services\Erir;


use InvalidArgumentException;
use Illuminate\Contracts\Config\Repository;

class ErirManager
{
    public function __construct(
        protected Repository $config,
        protected ErirFactory $factory,
        protected array $connections = [],
        protected array $extensions = [],
        protected string $configName = 'erir',
    )
    {}

    public function connection(string $name = null): mixed
    {
        $name = $name ?: $this->getDefaultConnection();

        if (!isset($this->connections[$name])) {
            $this->connections[$name] =  $this->makeConnection($name);
        }

        return $this->connections[$name];
    }

    public function makeConnection(string $name): mixed
    {
        $config = $this->getConnectionConfig($name);

        return $this->factory->make($config);
    }

    public function getConnectionConfig(string $name): array
    {
        $connections = $this->config->get(sprintf('%s.connections', $this->configName));

        if (!is_array($connections)) {
            throw new InvalidArgumentException('Connections are not configured.');
        }
        $config = $connections[$name];

        if (!$config) {
            throw new InvalidArgumentException(sprintf(
                'Connection %s not configured.',
                $name
            ));
        }

        return array_merge(['name' => $name], $config);
    }

    public function getDefaultConnection(): mixed
    {
        return $this->config->get(sprintf('%s.default', $this->configName));
    }

    public function setDefaultConnection(string $name): void
    {
        $this->config->set(sprintf('%s.default', $this->configName), $name);
    }

    public function getConnections(): array
    {
        return $this->connections;
    }

    public function getConfig(): Repository
    {
        return $this->config;
    }

    public function __call(string $method, array $arguments)
    {
        return $this->connection()->$method(...$arguments);
    }
}