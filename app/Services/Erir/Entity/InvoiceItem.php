<?php


namespace App\Services\Erir\Entity;


class InvoiceItem
{
    public function __construct(
        public mixed $amount,
        public string $startDate,
        public string $endDate,
        public string $externalCampaignId,
    )
    {}
}