<?php


namespace App\Services\Erir\Entity;


class StatisticItem
{
    public function __construct(
        public int $clicks,
        public int $convs,
        public int $imps
    )
    {}
}