<?php

namespace App\Services\Erir\Api;

use App\Services\Erir\Entity\StatisticItem;

class Statistics extends AbstractApi
{

    protected string $endpoint = 'statistics';

    /**
     * @param StatisticItem[] $stat
     */
    public function create(
        string $campaignId,
        string $creativeId,
        string $startDate,
        string $endDate,
        array $stat
    ): bool
    {
        return $this->postCreate($this->getEndpoint(), [[
            'startDate' => $startDate,
            'endDate' => $endDate,
            'externalCampaignId' => $campaignId,
            'externalCreativeId' => $creativeId,
            'stat' => $stat
        ]]);
    }
}
