<?php

namespace App\Services\Erir\Api;

class Contracts extends AbstractApi
{

    protected string $endpoint = 'contract';

    public function create(
        string $id,
        string $contractorId,
        string $clientId,
        mixed $amount,
        string $date,
        string $documentLink,
        string $number,
        bool $isVat,
        ?string $contractId = null,
    ): bool
    {
        $data = [
            'amount' => $amount,
            'date' => $date,
            'documentLink' => $documentLink,
            'externalClientId' => $clientId,
            'externalContractorId' => $contractorId,
            'externalId' =>  $id,
            'isVat' => $isVat,
            'number' => $number
        ];

        if ($contractId) {
            $data['externalContractId'] = $contractId;
        }
        return $this->postCreate($this->getEndpoint(), $data);
    }
}
