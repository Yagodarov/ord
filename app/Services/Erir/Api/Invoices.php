<?php

namespace App\Services\Erir\Api;

use App\Services\Erir\Entity\InvoiceItem;

class Invoices extends AbstractApi
{

    protected string $endpoint = 'invoice';

    /**
     * @param InvoiceItem[] $invoiceItems
     */
    public function create(
        string $id,
        string $contractId,
        mixed $amount,
        string $documentLink,
        string $startDate,
        string $endDate,
        array $invoiceItems,
        bool $isVat
    ): bool
    {
        return $this->postCreate($this->getEndpoint(), [
            'externalId' => $id,
            'externalContractId' => $contractId,
            'amount' => $amount,
            'documentLink' =>$documentLink,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'invoiceItems' => $invoiceItems,
            'isVat' => $isVat,
        ]);
    }
}
