<?php

namespace App\Services\Erir\Api;

class Agencies extends AbstractApi
{

    protected string $endpoint = 'agency';

    public function create(string $id, string $name, string $inn): bool
    {
        return $this->postCreate($this->getEndpoint(), [
            'externalId' => $id,
            'inn' => $inn,
            'name' => $name
        ]);
    }
}
