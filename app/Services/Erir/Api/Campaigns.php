<?php

namespace App\Services\Erir\Api;

class Campaigns extends AbstractApi
{

    protected string $endpoint = 'campaign';

    public function create(string $id, string $advertiserId, string $startDate, string $endDate): bool
    {
        return $this->postCreate($this->getEndpoint(), [
            'externalId' => $id,
            'externalAdvertiserId' => $advertiserId,
            'startDate' => $startDate,
            'endDate' => $endDate,
        ]);
    }
}
