<?php

namespace App\Services\Erir\Api;

class Creatives extends AbstractApi
{

    protected string $endpoint = 'creative';

    public function create(
        string $category,
        array $mediaUrl,
        array $targetingRegion ,
        array $textData,
        string $campaignId,
        string $id,
        string $type,
        string $url,
    ): bool
    {
        return $this->postCreate($this->getEndpoint(), [
            'externalId' => $id,
            'externalCampaignId' => $campaignId,
            'category' => $category,
            'type' => $type,
            'url' => $url,
            'creativeData' => [
                'mediaUrl' => $mediaUrl,
                'targetingRegion' => $targetingRegion,
                'textData' => $textData
            ]
        ]);
    }
}
