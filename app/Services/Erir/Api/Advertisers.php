<?php

namespace App\Services\Erir\Api;

class Advertisers extends AbstractApi
{
    protected string $endpoint = 'advertiser';

    public function create(string $id, string $name, string $inn): bool
    {
        return $this->postCreate($this->getEndpoint(), [
            'externalId' => $id,
            'inn' => $inn,
            'name' => $name
        ]);
    }
}
