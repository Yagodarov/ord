<?php


namespace App\Services\Erir\Api;


use App\Services\Erir\ErirClient;
use App\Services\Erir\Exceptions\RuntimeException;
use App\Services\Erir\HttpClient\ResponseMediator;
use App\Services\Erir\HttpClient\Util\JsonArray;
use App\Services\Erir\HttpClient\Util\QueryStringBuilder;
use Http\Message\MultipartStream\MultipartStreamBuilder;
use Psr\Http\Message\StreamInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractApi
{
    protected const BASE_PATH = 'api/v1/register-external-service/';

    public function __construct(
        private ErirClient $client,
    )
    {}

    public function getById(string $id): array
    {
        return $this->get(sprintf('%s/%s', $this->getEndpoint(), $id));
    }

    protected function get(string $uri, array $params = [], array $headers = []): mixed
    {
        $response = $this->client->getHttpClient()->get(self::prepareUri($uri, $params), $headers);
        return ResponseMediator::getContent($response);
    }

    protected function post(string $uri, array $params = [], array $headers = [], array $files = []): mixed
    {
        if (0 < \count($files)) {
            $builder = $this->createMultipartStreamBuilder($params, $files);
            $body = self::prepareMultipartBody($builder);
            $headers = self::addMultipartContentType($headers, $builder);
        } else {
            $body = self::prepareJsonBody($params);

            if (null !== $body) {
                $headers = self::addJsonContentType($headers);
            }
        }

        $response = $this->client->getHttpClient()->post(self::prepareUri($uri), $headers, $body);

        return ResponseMediator::getContent($response);
    }

    protected function put(string $uri, array $params = [], array $headers = [], array $files = []): mixed
    {
        if (0 < \count($files)) {
            $builder = $this->createMultipartStreamBuilder($params, $files);
            $body = self::prepareMultipartBody($builder);
            $headers = self::addMultipartContentType($headers, $builder);
        } else {
            $body = self::prepareJsonBody($params);

            if (null !== $body) {
                $headers = self::addJsonContentType($headers);
            }
        }

        $response = $this->client->getHttpClient()->put(self::prepareUri($uri), $headers, $body ?? '');

        return ResponseMediator::getContent($response);
    }

    protected function delete(string $uri, array $params = [], array $headers = []): mixed
    {
        $body = self::prepareJsonBody($params);

        if (null !== $body) {
            $headers = self::addJsonContentType($headers);
        }

        $response = $this->client->getHttpClient()->delete(self::prepareUri($uri), $headers, $body ?? '');

        return ResponseMediator::getContent($response);
    }

    protected function postCreate(string $uri, array $params = [], array $headers = []): bool
    {
        $response = $this->client->getHttpClient()->post(
            self::prepareUri($uri),
            self::addJsonContentType($headers),
            self::prepareJsonBody($params)
        );

        if (!in_array($response->getStatusCode(), [201, 304])) {
            throw new RuntimeException(sprintf('Failed to create new %s entity. Code: %s. Message: %s',
                explode('/', $uri)[0],
                $response->getStatusCode(),
                (string) $response->getBody()
            ));
        }

        return in_array($response->getStatusCode(), [201, 304]);
    }

    protected function getEndpoint(): string
    {
        if (!$this->endpoint) {
            throw new RuntimeException(sprintf(
                'Failed to create connection for %s. Endpoint not defined in entity class.',
                self::class
            ));
        }

        return $this->endpoint;
    }

    protected static function encodePath($uri): string
    {
        return \rawurlencode((string) $uri);
    }

    protected function createOptionsResolver(): OptionsResolver
    {
        $resolver = new OptionsResolver();
        $resolver->setDefined('page')
            ->setAllowedTypes('page', 'int')
            ->setAllowedValues('page', function ($value): bool {
                return $value > 0;
            })
        ;
        $resolver->setDefined('per_page')
            ->setAllowedTypes('per_page', 'int')
            ->setAllowedValues('per_page', function ($value): bool {
                return $value > 0 && $value <= 100;
            })
        ;

        return $resolver;
    }

    private static function prepareUri(string $uri, array $query = []): string
    {
        $query = \array_filter($query, function ($value): bool {
            return null !== $value;
        });

        return \sprintf('%s%s%s', self::BASE_PATH, $uri, QueryStringBuilder::build($query));
    }

    private function createMultipartStreamBuilder(array $params = [], array $files = []): MultipartStreamBuilder
    {
        $builder = new MultipartStreamBuilder($this->client->getStreamFactory());

        foreach ($params as $name => $value) {
            $builder->addResource($name, $value);
        }

        foreach ($files as $name => $file) {
            $builder->addResource($name, self::tryFopen($file, 'r'), [
                'headers' => [
                    ResponseMediator::CONTENT_TYPE_HEADER => self::guessFileContentType($file),
                ],
                'filename' => \basename($file),
            ]);
        }

        return $builder;
    }

    private static function prepareMultipartBody(MultipartStreamBuilder $builder): StreamInterface
    {
        return $builder->build();
    }

    private static function addMultipartContentType(array $headers, MultipartStreamBuilder $builder): array
    {
        $contentType = \sprintf('%s; boundary=%s', ResponseMediator::MULTIPART_CONTENT_TYPE, $builder->getBoundary());

        return \array_merge([ResponseMediator::CONTENT_TYPE_HEADER => $contentType], $headers);
    }

    private static function prepareJsonBody(array $params): ?string
    {
        $params = \array_filter($params, function ($value): bool {
            return null !== $value;
        });

        if (0 === \count($params)) {
            return null;
        }

        return JsonArray::encode($params);
    }

    private static function addJsonContentType(array $headers): array
    {
        return \array_merge([ResponseMediator::CONTENT_TYPE_HEADER => ResponseMediator::JSON_CONTENT_TYPE], $headers);
    }

    private static function tryFopen(string $filename, string $mode)
    {
        $ex = null;
        \set_error_handler(function () use ($filename, $mode, &$ex): void {
            $ex = new RuntimeException(\sprintf(
                'Unable to open %s using mode %s: %s',
                $filename,
                $mode,
                \func_get_args()[1]
            ));
        });

        $handle = \fopen($filename, $mode);
        \restore_error_handler();

        if (null !== $ex) {
            throw $ex;
        }

        return $handle;
    }

    private static function guessFileContentType(string $file): string
    {
        if (!\class_exists(\finfo::class, false)) {
            return ResponseMediator::STREAM_CONTENT_TYPE;
        }

        $type = (new \finfo(\FILEINFO_MIME_TYPE))->file($file);

        return false !== $type ? $type : ResponseMediator::STREAM_CONTENT_TYPE;
    }

}