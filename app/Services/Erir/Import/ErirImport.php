<?php

namespace App\Services\Erir\Import;

use Illuminate\Contracts\Config\Repository;

class ErirImport
{
    public function __construct(
        protected Repository $config,
        protected ImportFactory $factory,
        protected string $providerName = 'erir',
        protected array $providers = [],
    )
    {}

    public function provider(string $provider = null): mixed
    {
        $provider = $provider ?: $this->getDefaultProvider();

        if (!isset($this->connections[$provider])) {
            $this->providers[$provider] =  $this->makeProvider($provider);
        }

        return $this->providers[$provider];
    }

    public function makeProvider(string $provider): mixed
    {
        $config = $this->getProviderConfig($provider);

        return $this->factory->make($config);
    }

    public function getProviderConfig(string $provider): array
    {
        $providers = $this->config->get(sprintf('%s.import', $this->providerName));

        if (!is_array($providers)) {
            throw new \InvalidArgumentException('Import providers are not configured.');
        }
        $config = $providers[$provider];

        if (!$config) {
            throw new \InvalidArgumentException(sprintf(
                'Import provider %s not configured.',
                $provider
            ));
        }

        return array_merge(['name' => $provider], $config);
    }

    public function getDefaultProvider(): string
    {
        return $this->config->get(sprintf('%s.default_import', $this->providerName));
    }

    public function setDefaultProvider(string $provider): void
    {
        $this->config->set(sprintf('%s.default_import', $this->providerName), $provider);
    }

    public function getConnections(): array
    {
        return $this->providers;
    }

    public function getConfig(): Repository
    {
        return $this->config;
    }

    public function __call(string $method, array $arguments)
    {
        return $this->provider()->$method(...$arguments);
    }
}
