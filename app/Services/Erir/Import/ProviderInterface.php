<?php

namespace App\Services\Erir\Import;

interface ProviderInterface
{
    public function next(): array|bool;
    public function setFile(string $importFile): self;
    public function setDetailFile(string $detailFile): self;
    public function setEntity(string $entity): self;
}

