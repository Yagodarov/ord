<?php

namespace App\Services\Erir\Import\Csv;

use App\Services\Erir\Exceptions\RuntimeException;
use App\Services\Erir\Import\ProviderInterface;

class Provider implements ProviderInterface
{

    public function __construct(
        protected ?Reader $reader = null,
        protected ?Reader $detailReader = null,
        protected ?Writer $writer = null,
        protected ?array $mapping = null,
        protected ?string $entity = null,
    )
    {
        $this->reader = new Reader();
        $this->detailReader = new Reader();
        $this->writer = new Writer();
    }

    public function setFile(string $importFile): self
    {
        $this->reader->setFile($importFile);
        return $this;
    }

    public function setDetailFile(string $detailFile): self
    {
        $this->detailReader->setFile($detailFile);
        return $this;
    }

    public function setEntity(string $entity): self
    {
        $this->entity = $entity;
        return $this;
    }

    public function setMapping(array $mapping): self
    {
        $this->mapping = $mapping;
        return $this;
    }

    public function getDetailReader(): Reader
    {
        return $this->detailReader;
    }

    public function getReader(): Reader
    {
        return $this->reader;
    }

    public function getWriter(): Writer
    {
        return $this->writer;
    }

    public function getTotalCount(): int
    {
        $lastLine = $this->reader->getLastLineNumber();
        return !$lastLine ? $lastLine : $lastLine + 1;
    }

    public function next(): array|bool
    {
        $data = [];
        $row = $this->reader->getRow();
        if (!$row) return $row;

        if ($this->mapping && !key_exists($this->entity, $this->mapping)) {
            throw new RuntimeException(sprintf(
                'Config for import mapping %s don\'t exists',
                $this->entity
            ));
        }

        $mapping = $this->mapping[$this->entity]['fields'];

        if (count($row) !== count($mapping)) {
            throw new RuntimeException(sprintf(
                'Count of import mapping fields (%s) does not match with import file columns (%s) count',
                count($mapping),
                count($row)
            ));
        }

        foreach ($mapping as $i => $key) {
            $data[$key] = $row[$i];
        }

        return $data;
    }
}

