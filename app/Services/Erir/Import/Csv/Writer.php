<?php

namespace App\Services\Erir\Import\Csv;

class Writer extends AbstractCsv
{
    public function writeRow(string $row): bool|int
    {
        if (is_string($row)) {
            $row = explode(',', $row);
            $row = array_map('trim', $row);
        }

        return $this->getHandle()->fputcsv($row, $this->delimiter, $this->enclosure);
    }

    public function writeFromArray(array $array) : void
    {
        foreach ($array as $key => $value) {
            $this->writeRow($value);
        }
    }
}
