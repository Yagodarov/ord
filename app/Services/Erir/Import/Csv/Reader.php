<?php

namespace App\Services\Erir\Import\Csv;

use App\Services\Erir\Exceptions\RuntimeException;

class Reader extends AbstractCsv
{
    public function __construct(
        protected string $mode = 'r+',
        protected ?string $path = null,
        private bool $headersInFirstRow = false,
        private bool|int $headerLine = false,
        private bool|int $lastLine = false,
        private array $headers = [],
        private bool $isNeedBOMRemove = true,
        private bool $init = false,
    )
    {
        parent::__construct($mode, $path);
    }

    public function setFile(string $importFile): self
    {
        $this->path = $importFile;
        return $this;
    }

    public function setEntity(string $entity): self
    {
        $this->entity = $entity;
        return $this;
    }

    public function getHeaders(): array
    {
        $this->init();

        return $this->headers;
    }

    public function getRow(): array|bool
    {
        $this->init();

        if ($this->isEof()) {
            return false;
        }

        $row     = $this->getCurrentRow();
        $isEmpty = $this->rowIsEmpty($row);

        if ($this->isEof() === false) {
            $this->getHandle()->next();
        }

        $hasHeaders = $this->headers !== false && is_array($this->headers) && count($this->headers);

        if ($isEmpty === false) {
            return $hasHeaders ? array_combine($this->headers, $row) : $row;
        }

        if ($hasHeaders && (count($this->headers) !== count($row))) {
            return $this->getRow();
        }

        if (is_array($this->headers) && count($this->headers)) {
            return array_combine($this->headers, $row);
        }

        return false;
    }

    public function isEof(): bool
    {
        return $this->getHandle()->eof();
    }

    public function getAll(): array
    {
        $data = [];
        while ($row = $this->getRow()) {
            $data[] = $row;
        }

        return $data;
    }

    public function getLineNumber(): int
    {
        return $this->getHandle()->key();
    }

    public function getLastLineNumber(): mixed
    {
        if ($this->lastLine !== false) {
            return $this->lastLine;
        }

        $this->getHandle()->seek($this->getHandle()->getSize());
        $lastLine = $this->getHandle()->key();

        $this->getHandle()->rewind();

        return $this->lastLine = $lastLine;
    }

    public function getCurrentRow(): array
    {
        $current = $this->getHandle()->current();

        if (! is_string($current)) {
            return [];
        }

        if ($this->isNeedBOMRemove && mb_strpos($current, "\xEF\xBB\xBF", 0, 'utf-8') === 0) {
            $this->isNeedBOMRemove = false;

            $current = str_replace("\xEF\xBB\xBF", '', $current);
        }

        return str_getcsv($current, $this->delimiter, $this->enclosure);
    }

    public function advanceTo(int $lineNumber): void
    {
        if ($this->headerLine > $lineNumber) {
            throw new RuntimeException(sprintf(
                'Line Number %s is before the header line that was set',
                $lineNumber
            ));
        }

        if ($this->headerLine === $lineNumber) {
            throw new RuntimeException(sprintf(
                'Line Number %s is equal to the header line that was set',
                $lineNumber
            ));
        }

        if ($lineNumber > 0) {
            $this->getHandle()->seek($lineNumber - 1);
        } // check the line before

        if ($this->isEof()) {
            throw new RuntimeException(sprintf(
                'Line Number %s is past the end of the file',
                $lineNumber
            ));
        }

        $this->getHandle()->seek($lineNumber);
    }

    public function setHeaderLine(int $lineNumber): bool
    {
        if ($lineNumber === 0) {
            return false;
        }

        $this->headersInFirstRow = false;

        $this->headerLine = $lineNumber;

        $this->getHandle()->seek($lineNumber);

        $this->headers = $this->getHeadersFromRow();

        return true;
    }

    protected function init(): void
    {
        if ($this->init === true) {
            return;
        }

        $this->init = true;

        if ($this->headersInFirstRow !== true) {
            return;
        }

        $this->getHandle()->rewind();

        $this->headerLine = 0;

        $this->headers = $this->getHeadersFromRow();
    }

    protected function rowIsEmpty(array $row): bool
    {
        $emptyRow = ($row === [null]);
        $emptyRowWithDelimiters = (array_filter($row) === []);
        $isEmpty = false;

        if ($emptyRow) {
            return true;
        }

        if ($emptyRowWithDelimiters) {
            return true;
        }

        return $isEmpty;
    }

    private function getHeadersFromRow(): array
    {
        $row = $this->getRow();
        return is_array($row) ? $row : [];
    }
}
