<?php

namespace App\Services\Erir\Import\Csv;

class AbstractCsv
{
    public function __construct(
        protected string $mode = 'r+',
        protected ?string $path = null,
        protected bool $isNeedBOM = false,
        protected array $initialHeaders = [],
        protected string $delimiter = ',',
        protected string $enclosure = '"',
        protected ?\SplFileObject $handle = null
    ) {}

    public function __destruct()
    {
        $this->handle = null;
    }

    public function setDelimiter(string $delimiter) : void
    {
        $this->delimiter = $delimiter;
    }

    public function setEnclosure(string $enclosure) : void
    {
        $this->enclosure = $enclosure;
    }

    protected function getHandle() : \SplFileObject
    {
        if ($this->handle === null) {
            $this->handle = $this->initializeHandle();
        }

        return $this->handle;
    }

    private function initializeHandle() : \SplFileObject
    {
        $isNewFile = false;

        if (! file_exists($this->path)) {
            touch($this->path);

            $isNewFile = true;
        }

        $handle = new \SplFileObject($this->path, $this->mode);
        $handle->setFlags(\SplFileObject::DROP_NEW_LINE);

        if ($this->isNeedBOM) {
            $handle->fwrite("\xEF\xBB\xBF");
        }

        if ($isNewFile && $this->initialHeaders !== []) {
            $headerLine = implode(',', $this->initialHeaders) . PHP_EOL;

            $handle->fwrite($headerLine, strlen($headerLine));
        }

        return $handle;
    }
}
