<?php

namespace App\Services\Erir\Import\Provider;

use App\Services\Erir\Import\Csv\Provider;
use App\Services\Erir\Import\FileImporter;

class CsvProvider extends AbstractProvider
{
    public function handle(array $config): FileImporter
    {
        $provider = new Provider();

        if (key_exists('mapping', $config)) {
            $provider->setMapping($config['mapping']);
        }

        if (key_exists('delimiter', $config)) {
            $provider->getReader()->setDelimiter($config['delimiter']);
        }

        return $this->importer
            ->setProvider($provider);
    }
}
