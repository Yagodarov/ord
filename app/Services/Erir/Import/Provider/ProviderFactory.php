<?php

namespace App\Services\Erir\Import\Provider;

class ProviderFactory
{
    public function make(string $name)
    {
        return match ($name) {
            'csv' => new CsvProvider(),
            default =>  throw new \InvalidArgumentException(sprintf(
                'Unsupported provider name [%s].',
                $name
            ))
        };
    }
}
