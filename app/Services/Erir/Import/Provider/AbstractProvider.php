<?php

namespace App\Services\Erir\Import\Provider;

use App\Services\Erir\Import\FileImporter;

abstract class AbstractProvider
{
    public FileImporter $importer;

    public function withImporter($importer): self
    {
        $this->importer = $importer;
        return $this;
    }
}

