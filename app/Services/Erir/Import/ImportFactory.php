<?php

namespace App\Services\Erir\Import;

use App\Services\Erir\Import\Provider\ProviderFactory;
use App\Services\Erir\ErirManager;

class ImportFactory
{
    public function __construct(
        protected ProviderFactory $provider,
        protected ErirManager $api
    )
    {}

    public function make(array $config): FileImporter
    {
        $importer = new FileImporter($this->api);

        if (in_array($config['name'], ['csv', 'xls']) && !array_key_exists('mapping', $config)) {
            throw new \InvalidArgumentException(sprintf(
                'Erir import factory %s requires a mapping parameter.',
                $config['name']
            ));
        }

        return $this->provider->make($config['name'])
                ->withImporter($importer)
                ->handle($config);
    }
}
