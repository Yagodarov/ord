<?php

namespace App\Services\Erir\Import;

use App\Services\Erir\ErirClient;
use App\Services\Erir\ErirManager;
use App\Services\Erir\Exceptions\RuntimeException;
use Illuminate\Support\Facades\Log;

class FileImporter
{
    public function __construct(
        protected ErirManager $api,
        protected ?ProviderInterface $provider = null,
    )
    {}

    public function setProvider(ProviderInterface $provider): self
    {
        $this->provider = $provider;
        return $this;
    }

    public function __call(string $name, array $arguments)
    {
        $import = $arguments[0];
        $this->checkData($name, $import);

        $this->provider
            ->setFile($import['from'])
            ->setEntity($name);

        if ($import['detail']) {
            $this->provider->setDetailFile($import['detail']);
        }

        $totalRows = $this->provider->getTotalCount();
        $currentRow = 1;

        while ($data = $this->provider->next()) {
            try {
                $result = $this->api->$name()->create(...$data);
            } catch (RuntimeException $e) {
                Log::channel('stderr')->error(sprintf('Ошибка отправки данных импорта %s',
                    $e->getMessage()
                ));
            }
            Log::channel('stderr')->info(sprintf('Отправка данных %s [%s из %s] - %s',
                $name, $currentRow, $totalRows, $result ? 'успешно' : 'ошибка'
            ));
            $currentRow ++;
        }
    }

    private function checkData(string $name, array $import): void
    {
        if (!$import['from'] || !file_exists($import['from'])) {
            throw new RuntimeException(sprintf(
                'Import file %s for %s doesn\'t exists', $import['from'], $name
            ));
        }

        if ($import['detail'] && !file_exists($import['detail'])) {
            throw new RuntimeException(sprintf(
                'Details import file %s for %s doesn\'t exists', $import['detail'], $name
            ));
        }

        if (!method_exists(ErirClient::class, $name)) {
            throw new RuntimeException(sprintf(
                'Import class for object %s doesn\'t exists',
                $name
            ));
        }

        if (!method_exists($this->api->$name(), 'create')) {
            throw new RuntimeException(sprintf(
                'Import class for object %s doesn\'t have create function',
                $name
            ));
        }
    }
}
