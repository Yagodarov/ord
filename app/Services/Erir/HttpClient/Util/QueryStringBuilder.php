<?php

declare(strict_types=1);

namespace App\Services\Erir\HttpClient\Util;


final class QueryStringBuilder
{

    public static function build(array $query): string
    {
        return \sprintf('?%s', \implode('&', \array_map(function ($value, $key): string {
            return self::encode($value, $key);
        }, $query, \array_keys($query))));
    }

    private static function encode($query, $prefix): string
    {
        if (!\is_array($query)) {
            return self::rawurlencode($prefix).'='.self::rawurlencode($query);
        }

        $isList = self::isList($query);

        return \implode('&', \array_map(function ($value, $key) use ($prefix, $isList): string {
            $prefix = $isList ? $prefix.'[]' : $prefix.'['.$key.']';

            return self::encode($value, $prefix);
        }, $query, \array_keys($query)));
    }

    private static function isList(array $query): bool
    {
        if (0 === \count($query) || !isset($query[0])) {
            return false;
        }

        return \array_keys($query) === \range(0, \count($query) - 1);
    }

    private static function rawurlencode($value): string
    {
        if (false === $value) {
            return '0';
        }

        return \rawurlencode((string) $value);
    }
}
