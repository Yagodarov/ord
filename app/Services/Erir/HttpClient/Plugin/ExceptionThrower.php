<?php

declare(strict_types=1);


namespace App\Services\Erir\HttpClient\Plugin;

use App\Services\Erir\Exceptions\ExceptionInterface;
use App\Services\Erir\Exceptions\RuntimeException;
use App\Services\Erir\Exceptions\ValidationFailedException;
use App\Services\Erir\HttpClient\ResponseMediator;
use Http\Client\Common\Plugin;
use Http\Promise\Promise;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

final class ExceptionThrower implements Plugin
{

    public function handleRequest(RequestInterface $request, callable $next, callable $first): Promise
    {
        return $next($request)->then(function (ResponseInterface $response): ResponseInterface {

            $status = $response->getStatusCode();

            if ($status >= 400 && $status < 600) {
                throw self::createException($status, ResponseMediator::getErrorMessage($response) ?? $response->getReasonPhrase());
            }

            return $response;
        });
    }

    private static function createException(int $status, string $message): ExceptionInterface
    {
        if (400 === $status || 422 === $status) {
            return new ValidationFailedException($message, $status);
        }

        return new RuntimeException($message, $status);
    }
}
