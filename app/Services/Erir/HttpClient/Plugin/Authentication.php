<?php

declare(strict_types=1);


namespace App\Services\Erir\HttpClient\Plugin;

use App\Services\Erir\ErirClient;
use App\Services\Erir\Exceptions\RuntimeException;
use Http\Client\Common\Plugin;
use Http\Promise\Promise;
use Psr\Http\Message\RequestInterface;

final class Authentication implements Plugin
{
    private $headers;

    public function __construct(string $method, string $token)
    {
        $this->headers = self::buildHeaders($method, $token);
    }

    public function handleRequest(RequestInterface $request, callable $next, callable $first): Promise
    {
        foreach ($this->headers as $header => $value) {
            $request = $request->withHeader($header, $value);
        }

        return $next($request);
    }

    private static function buildHeaders(string $method, string $token): array
    {
        $headers = [];

        switch ($method) {

            case ErirClient::AUTH_TOKEN:
                $headers['Authorization'] = \sprintf('Bearer %s', $token);
                break;
            default:
                throw new RuntimeException(
                    \sprintf('Authentication method "%s" not implemented.', $method)
                );
        }

        return $headers;
    }
}
