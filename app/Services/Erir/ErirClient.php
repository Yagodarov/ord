<?php


namespace App\Services\Erir;


use App\Services\Erir\Api\Advertisers;
use App\Services\Erir\Api\Agencies;
use App\Services\Erir\Api\Campaigns;
use App\Services\Erir\Api\Contracts;
use App\Services\Erir\Api\Creatives;
use App\Services\Erir\Api\Invoices;
use App\Services\Erir\Api\Statistics;
use App\Services\Erir\HttpClient\Builder;
use App\Services\Erir\HttpClient\Plugin\Authentication;
use App\Services\Erir\HttpClient\Plugin\ExceptionThrower;
use App\Services\Erir\HttpClient\Plugin\History;
use Http\Client\Common\HttpMethodsClientInterface;
use Http\Client\Common\Plugin\AddHostPlugin;
use Http\Client\Common\Plugin\HeaderDefaultsPlugin;
use Http\Client\Common\Plugin\HistoryPlugin;
use Http\Client\Common\Plugin\RedirectPlugin;
use Psr\Http\Client\ClientInterface;
use JetBrains\PhpStorm\Pure;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;

class ErirClient
{
    public const AUTH_TOKEN = 'token';

    private const USER_AGENT = 'ord-erir-api-client';

    public function __construct(Builder $httpClientBuilder = null)
    {
        $this->httpClientBuilder = $builder = $httpClientBuilder ?? new Builder();
        $this->responseHistory = new History();

        $builder->addPlugin(new ExceptionThrower());
        $builder->addPlugin(new HistoryPlugin($this->responseHistory));
        $builder->addPlugin(new HeaderDefaultsPlugin([
            'User-Agent' => self::USER_AGENT,
        ]));
        $builder->addPlugin(new RedirectPlugin());
    }

    public static function createWithHttpClient(ClientInterface $httpClient): self
    {
        $builder = new Builder($httpClient);
        return new self($builder);
    }


    #[Pure]
    public function advertisers(): Advertisers
    {
        return new Advertisers($this);
    }

    #[Pure]
    public function agencies(): Agencies
    {
        return new Agencies($this);
    }

    #[Pure]
    public function campaigns(): Campaigns
    {
        return new Campaigns($this);
    }

    #[Pure]
    public function contracts(): Contracts
    {
        return new Contracts($this);
    }

    #[Pure]
    public function creatives(): Creatives
    {
        return new Creatives($this);
    }

    #[Pure]
    public function invoices(): Invoices
    {
        return new Invoices($this);
    }

    #[Pure]
    public function Statistics(): Statistics
    {
        return new Statistics($this);
    }


    public function authenticate(string $token, string $authMethod): void
    {
        $this->getHttpClientBuilder()->removePlugin(Authentication::class);
        $this->getHttpClientBuilder()->addPlugin(new Authentication($authMethod, $token));
    }

    public function setUrl(string $url): void
    {
        $uri = $this->getHttpClientBuilder()->getUriFactory()->createUri($url);

        $this->getHttpClientBuilder()->removePlugin(AddHostPlugin::class);
        $this->getHttpClientBuilder()->addPlugin(new AddHostPlugin($uri));
    }

    public function getLastResponse(): ?ResponseInterface
    {
        return $this->responseHistory->getLastResponse();
    }

    public function getHttpClient(): HttpMethodsClientInterface
    {
        return $this->getHttpClientBuilder()->getHttpClient();
    }

    public function getStreamFactory(): StreamFactoryInterface
    {
        return $this->getHttpClientBuilder()->getStreamFactory();
    }

    protected function getHttpClientBuilder(): Builder
    {
        return $this->httpClientBuilder;
    }
}