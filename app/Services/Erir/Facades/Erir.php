<?php

declare(strict_types=1);

namespace App\Services\Erir\Facades;

use Illuminate\Support\Facades\Facade;

class Erir extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'erir';
    }
}
