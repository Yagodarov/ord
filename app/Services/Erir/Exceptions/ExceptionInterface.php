<?php

declare(strict_types=1);

namespace App\Services\Erir\Exceptions;

use Http\Client\Exception;

interface ExceptionInterface extends Exception
{
}
