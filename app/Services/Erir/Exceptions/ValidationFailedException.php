<?php

declare(strict_types=1);

namespace App\Services\Erir\Exceptions;

class ValidationFailedException extends ErrorException
{
}
