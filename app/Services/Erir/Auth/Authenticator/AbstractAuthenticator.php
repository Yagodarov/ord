<?php


namespace App\Services\Erir\Auth\Authenticator;


use App\Services\Erir\ErirClient;

abstract class AbstractAuthenticator
{
    public ErirClient $client;

    public function withClient(ErirClient $client): self
    {
        $this->client = $client;

        return $this;
    }
}