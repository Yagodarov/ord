<?php


namespace App\Services\Erir\Auth\Authenticator;


use App\Services\Erir\ErirClient;

class TokenAuthenticator extends AbstractAuthenticator
{
    public function authenticate(array $config)
    {
        if (!$this->client) {
            throw new \InvalidArgumentException('The client instance was not given to the authenticator.');
        }

        if (!array_key_exists('token', $config) || is_null($config['token'])) {
            throw new \InvalidArgumentException('The token authenticator requires a token.');
        }

        $this->client->authenticate($config['token'], ErirClient::AUTH_TOKEN);

        return $this->client;
    }
}