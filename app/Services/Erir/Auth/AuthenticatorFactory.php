<?php


namespace App\Services\Erir\Auth;


class AuthenticatorFactory
{
    public function make(string $method)
    {
        return match ($method) {
            'token', 'http_token' => new Authenticator\TokenAuthenticator(),
            default =>  throw new \InvalidArgumentException(sprintf(
                'Unsupported authentication method [%s].',
                $method
            ))
        };
    }
}