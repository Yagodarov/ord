<?php

namespace App\Services;

use App\Http\Resources\ContractCollection;
use App\Models\Advertiser;
use App\Models\Agency;
use App\Models\TargetingRegion;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;

class TargetingRegionService extends BaseService
{
    public function getRules(Request $request)
    {
        return [
            'name' => ['sometimes','string','unique','max:64'],
        ];
        // TODO: Implement getRules() method.
    }

    public function __construct()
    {
        $this->className = TargetingRegion::class;
    }

}
