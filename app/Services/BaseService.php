<?php
namespace App\Services;

use App\Http\Resources\BaseResource;
use App\Models\Advertiser;
use App\Models\BaseModel;
use App\Models\Role;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

abstract class BaseService
{
    public string $className;

    public abstract function getRules(Request $request);
    public abstract function getCollection($array);
    public function getResource($model) {
        return new BaseResource($model);
    }
    public function sendResponse($message, $code, $status = 'OK')
    {
        return response()->json([
            'data' => $message,
            'code' => $code,
            'status' => $status
        ]);
    }

    public function getStatus($code) {

    }

    public function list(Request $request)
    {
        $page = $request->get('page') ?? 1;
        $limit = $request->get('limit') ?? 20;
        /**
         * @var $models Builder
         */
        $query = (new $this->className)::query();
        $query = $this->getListQuery($query);
        $query = $query
            ->limit($limit)
            ->simplePaginate($limit, '*','page',$page);
        return $this->sendResponse($this->getCollection($query), 200);
    }

    public function getListQuery(\Illuminate\Database\Eloquent\Builder $query)
    {
        return $query->where('user_id','=',Auth::user()->getAuthIdentifier());
    }

    public function create(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), $this->getRules($request));
        if ($validator->fails()) {
            return $this->sendResponse($validator->errors(), 403, 'error');
        }
        $model = $this->createModel($request);
        if ($result = $model->save()) {
            return $this->sendResponse($model->getAttributes(), 200);
        }
        return $this->sendResponse($result, 200);
    }

    public function createModel(Request $request)
    {
        $model = new $this->className;
        $model->fill($request->all());
        $this->beforeCreate($model);
        return $model;
    }

    public function update(Request $request): JsonResponse
    {
        /**
         * @var $model BaseModel
         */
        $model = $this->findModel($request);
        $validator = Validator::make($request->all(), $this->getRules($request));
        if ($validator->fails()) {
            return $this->sendResponse($validator->errors(), 403, 'error');
        }
        if (!$model) {
            return $this->sendResponse("Not found", 404, 'error');
        }
        $model->fill($request->all());
        if ($result = $model->save()) {
            return $this->sendResponse($model->getAttributes(), 200);
        }
        return $this->sendResponse($result, 200);
    }

    public function delete(Request $request)
    {
        return $this->sendResponse($this->findModel($request)->delete(), 200);
    }

    public function view(Request $request)
    {
        $resource = $this->getResource($this->findModel($request));
        return $this->sendResponse($resource, 200);
    }

    public function getByExternalId(Request $request)
    {
        return $this->sendResponse($this->className::where('external_id','=',$request->route('external_id'))->getAttributes(), 200);
    }

    public function beforeCreate(BaseModel $model)
    {
        $model->setAttribute('user_id', Auth::user()->getAuthIdentifier());
    }

    public function findModel(Request $request)
    {
        $model = $this->getModel($request->route('id'));
        if (!$model) {
            throw new NotFoundHttpException('Model not found');
        }
        return $model;
    }

    public function checkPermissionTo($permission)
    {
        if (!Auth::user()->hasPermissionTo($permission)) {
            throw new NotFoundHttpException('User has no permission to do this action');
        }
    }

    public function getModel(int $id)
    {
        $query = $this->className::query();
        $query = $this->getModelByRole($query);
        $model = $query->find($id);
        return $model;
    }

    public function getmodelByRole(\Illuminate\Database\Eloquent\Builder $query)
    {
        $role = Auth::user()->getRole();
        if ($role == Role::admin) {
            return $query;
        }
        if ($role == Role::aggregator) {
            $query->where('user_id','=',Auth::user()->getAuthIdentifier());
        }
        if ($role == Role::agency) {
            $query->where('user_id','=',Auth::user()->getAuthIdentifier());
        }
        return $query;
    }

    public function getRole()
    {
        return Auth::user()->getRole();
    }
}
