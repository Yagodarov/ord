<?php

namespace App\Models;

class Creative extends BaseModel
{
    public $fillable = [
        'category',
        'descriptionObject',
        'domainSource',
        'urlSource',
        'systemAdv',
        'sex',
        'age',
        'url',
        'mediaUrl',
        'textData',
        'targeting_region_id',
        'campaign_id',
        'externalId',
    ];
}
