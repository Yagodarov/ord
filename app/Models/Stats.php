<?php

namespace App\Models;

class Stats extends BaseModel
{
    public $fillable = [
        'campaign_id',
        'creative_id',
        'clicks',
        'convs',
        'imps',
        'date',
    ];
}
