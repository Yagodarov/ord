<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @mixin Model
 */
class Agency extends BaseModel
{
    public $fillable = [
        'inn',
        'name',
        'ogrn',
        'address',
        'citizenName',
        'numberPhone',
        'numberPay',
        'codeCountry',
        'regNumberFor',
        'innFor',
    ];

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
