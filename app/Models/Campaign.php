<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Campaign extends BaseModel
{
    public $fillable = [
        'startDate',
        'endDate',
        'contract_id',
        'externalId',
        'type',
    ];

    public function agency()
    {
        return $this->belongsTo(Agency::class);
    }
}
