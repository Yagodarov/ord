<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends BaseModel
{
    public $fillable = [
        'amount',
        'startDate',
        'endDate',
        'isVat',
        'campaign_id',
        'contract_id',
        'externalId',
    ];

    public function contract()
    {
        return $this->belongsTo(Contract::class);
    }
}
