<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contract extends BaseModel
{
    public $fillable = [
        'amount',
        'subjectContract',
        'date',
        'isVat',
        'number',
        'agency_id',
        'advertiser_id',
        'externalId',
        'externalContractorId',
        'externalClientId',
    ];

    public function agency()
    {
        return $this->belongsTo(Agency::class);
    }
}
