<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TargetingRegion extends BaseModel
{
    public $fillable = [
        'name',
    ];
}
