<?php
namespace App\Models\logs;

use Illuminate\Database\Eloquent\Model;

/** @mixin Model */
class RequestLog extends Model
{
    protected $fillable = [
        'user_id',
        'request_body',
        'response_body',
        'request_method',
        'url',
        'ip_address',
    ];
}
