<?php

namespace App\Rules;

use App\Models\Contract;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class CheckContractId implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->needCheck()) {
            $contract = Contract
                ::query()
                ->where('id','=',$value)
                ->where('user_id','=',Auth::user()->getAuthIdentifier())
                ->first();
            if (!$contract) {
                return false;
            }
        }
        return true;
    }

    public function needCheck()
    {
        if (Auth::user()->getRole() == 'admin') {
            return false;
        }
        return true;
    }
    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Contract id is invalid';
    }
}
