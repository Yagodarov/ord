<?php

namespace App\Rules;

use App\Models\Agency;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class CheckAgencyId implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->needCheck()) {
            $agency = Agency
                ::query()
                ->where('id','=',$value)
                ->where('user_id','=',Auth::user()->getAuthIdentifier())->first();
            if (!$agency) {
                return false;
            }
        }
        return true;
    }

    public function needCheck()
    {
        if (Auth::user()->getRole() == 'admin') {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Agency id is invalid';
    }
}
