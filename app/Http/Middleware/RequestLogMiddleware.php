<?php

namespace App\Http\Middleware;

use App\Models\logs\RequestLog;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use function json_encode;

class RequestLogMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);
        $user = Auth::user();
        $attributes = [
            'user_id' => $user ? $user->getAttribute('id') : 0,
            'request_body' => json_encode($request->toArray()),
            'response_body' => json_encode($response->getData()),
            'request_method' => $request->method(),
            'url' => $request->path(),
            'ip_address' => $request->ip(),
        ];
        $requestLog = RequestLog::create($attributes);

        return $response;
    }
}
