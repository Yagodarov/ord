<?php

namespace App\Http\Controllers;

use App\Http\Resources\StatsCollection;
use App\Services\StatsService;
use Illuminate\Http\Request;

class StatsController extends BaseController
{
    public function __construct()
    {
        $this->service = new StatsService();
    }

    public function getCollection($array)
    {
        new StatsCollection($array);
    }

    public function list(Request $request)
    {
        return $this->service->list($request);
    }

    public function create(Request $request)
    {
        return $this->service->create($request);
    }

    public function update(Request $request)
    {
        return $this->service->update($request);
    }

    public function delete(Request $request)
    {
        return $this->service->delete($request);
    }

    public function view(Request $request)
    {
        return $this->service->view($request);
    }
}
