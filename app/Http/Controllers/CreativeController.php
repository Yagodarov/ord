<?php

namespace App\Http\Controllers;

use App\Http\Resources\AgencyCollection;
use App\Services\CreativeService;
use Illuminate\Http\Request;

class CreativeController extends BaseController
{
    public function __construct()
    {
        $this->service = new CreativeService();
    }

    public function getCollection($array)
    {
        new AgencyCollection($array);
    }

    public function list(Request $request)
    {
        return $this->service->list($request);
    }

    public function create(Request $request)
    {
        return $this->service->create($request);
    }

    public function update(Request $request)
    {
        return $this->service->update($request);
    }

    public function delete(Request $request)
    {
        return $this->service->delete($request);
    }

    public function view(Request $request)
    {
        return $this->service->view($request);
    }

    public function external(Request $request)
    {
        return $this->service->getByExternalId($request);
    }
}
