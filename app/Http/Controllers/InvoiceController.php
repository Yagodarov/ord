<?php

namespace App\Http\Controllers;

use App\Http\Resources\AdvertiserCollection;
use App\Http\Resources\InvoiceCollection;
use App\Services\CampaignService;
use App\Services\ContractService;
use App\Services\InvoiceService;

class InvoiceController extends BaseController
{
    public function __construct()
    {
        $this->service = new InvoiceService();
    }
    public function getCollection($array)
    {
        new InvoiceCollection($array);
    }
}
