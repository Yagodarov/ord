<?php

namespace App\Http\Controllers;

use App\Http\Resources\AdvertiserCollection;
use App\Services\AdvertiserService;

class AdvertiserController extends BaseController
{
    public function __construct()
    {
        $this->service = new AdvertiserService();
    }
    public function getCollection($array)
    {
        new AdvertiserCollection($array);
    }
}
