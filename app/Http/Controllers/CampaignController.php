<?php

namespace App\Http\Controllers;

use App\Http\Resources\AdvertiserCollection;
use App\Http\Resources\CampaignCollection;
use App\Services\CampaignService;

class CampaignController extends BaseController
{
    public function __construct()
    {
        $this->service = new CampaignService();
    }
    public function getCollection($array)
    {
        new CampaignCollection($array);
    }
}
