<?php

namespace App\Http\Controllers;

use App\Http\Resources\AdvertiserCollection;
use App\Http\Resources\ContractCollection;
use App\Services\CampaignService;
use App\Services\ContractService;

class ContractController extends BaseController
{
    public function __construct()
    {
        $this->service = new ContractService();
    }
    public function getCollection($array)
    {
        new ContractCollection($array);
    }
}
