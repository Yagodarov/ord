<?php

namespace App\Http\Resources;

use App\Models\Advertiser;
use Illuminate\Http\Resources\Json\ResourceCollection;

class AdvertiserCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return $this->resource->map(static function (Advertiser $item) {
            return $item->toArray();
        });
    }
}
