<?php

namespace App\Http\Resources;

use App\Models\Advertiser;
use App\Models\Agency;
use App\Models\Campaign;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CampaignCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return $this->resource->map(static function (Campaign $item) {
            return $item->toArray();
        });
    }
}
