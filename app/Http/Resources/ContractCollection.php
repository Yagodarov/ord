<?php

namespace App\Http\Resources;

use App\Models\Advertiser;
use App\Models\Agency;
use App\Models\Contract;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ContractCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return $this->resource->map(static function (ContractResource $item) {
            return $item;
        });
    }
}
