let url = {
  "info": {
    "title": "Alpha api ORD",
    "version": "1.0.0"
  },
  "openapi": "3.0.0",
  "servers": [
    {
      "url": "http://85.193.83.199:8000/",
      "description": "",
      "variables": {}
    }
  ],
  "paths": {  
    "/api/advertiser": {
      "post": {
        "summary": "create",
        "operationId": "createcreative",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "requestBody": {
          "required": true,
          "content": {
            "multipart/form-data": {
              "schema": {
                "type": "object",
                "required": [
                  "inn",
                  "name"
                ],
                "properties": {
                  "inn": {
                    "type": "string",
                    "example": "444"
                  },
                  "name": {
                    "type": "string",
                    "example": "222"
                  }
                }
              },
              "example": [
                {
                  "key": "inn",
                  "value": "444",
                  "type": "text"
                },
                {
                  "key": "name",
                  "value": "222",
                  "type": "text"
                }
              ]
            }
          }
        },
        "tags": [
          "advertiser"
        ]
      }
    },
    "/api/advertisers": {
      "get": {
        "summary": "list",
        "operationId": "list",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "tags": [
          "advertiser"
        ]
      }
    },
    "/api/advertiser/{id}": {
      "parameters": [
        {
          "in": "path",
          "name": "id",
          "schema": {
            "type": "integer"
          },
          "required": true
        }
      ],
      "put": {
        "summary": "update",
        "operationId": "update",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "requestBody": {
          "required": true,
          "content": {
            "application/x-www-form-urlencoded": {
              "schema": {
                "type": "object",
                "required": [
                  "inn",
                  "name"
                ],
                "properties": {
                  "inn": {
                    "type": "string",
                    "example": "11"
                  },
                  "name": {
                    "type": "string",
                    "example": "22"
                  }
                }
              }
            }
          }
        },
        "tags": [
          "advertiser"
        ]
      },
      "get": {
        "summary": "view",
        "operationId": "view",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "tags": [
          "advertiser"
        ]
      },
      "delete": {
        "summary": "delete",
        "operationId": "delete",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "tags": [
          "advertiser"
        ]
      }
    },
    "/api/agency": {
      "post": {
        "summary": "create",
        "operationId": "create0",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "requestBody": {
          "required": true,
          "content": {
            "multipart/form-data": {
              "schema": {
                "type": "object",
                "required": [
                  "inn",
                  "name"
                ],
                "properties": {
                  "inn": {
                    "type": "string",
                    "example": "444"
                  },
                  "name": {
                    "type": "string",
                    "example": "4563"
                  }
                }
              },
              "example": [
                {
                  "key": "inn",
                  "value": "444",
                  "type": "text"
                },
                {
                  "key": "name",
                  "value": "4563",
                  "type": "text"
                }
              ]
            }
          }
        },
        "tags": [
          "agency"
        ]
      }
    },
    "/api/agencies": {
      "get": {
        "summary": "list",
        "operationId": "list1",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "tags": [
          "agency"
        ]
      }
    },
    "/api/agency/{id}": {
      "parameters": [
        {
          "in": "path",
          "name": "id",
          "schema": {
            "type": "integer"
          },
          "required": true
        }
      ],
      "put": {
        "summary": "update",
        "operationId": "update2agency",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "requestBody": {
          "required": true,
          "content": {
            "application/x-www-form-urlencoded": {
              "schema": {
                "type": "object",
                "required": [
                  "inn",
                  "name"
                ],
                "properties": {
                  "inn": {
                    "type": "string",
                    "example": "222"
                  },
                  "name": {
                    "type": "string",
                    "example": "111"
                  }
                }
              }
            }
          }
        },
        "tags": [
          "agency"
        ]
      },
      "get": {
        "summary": "view",
        "operationId": "view3",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "tags": [
          "agency"
        ]
      },
      "delete": {
        "summary": "delete",
        "operationId": "delete4",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "tags": [
          "agency"
        ]
      }
    },
    "/api/campaign": {
      "post": {
        "summary": "create",
        "operationId": "create0campaign",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "requestBody": {
          "required": true,
          "content": {
            "multipart/form-data": {
              "schema": {
                "type": "object",
                "required": [
                  "startDate",
                  "endDate",
                  "externalId",
                  "contract_id"
                ],
                "properties": {
                  "startDate": {
                    "type": "string",
                    "example": "2022-01-22 00:00:29"
                  },
                  "endDate": {
                    "type": "string",
                    "example": "2022-01-22 13:23:29"
                  },
                  "externalId": {
                    "type": "string",
                    "example": "1341324"
                  },
                  "contract_id": {
                    "type": "string",
                    "example": "10"
                  }
                }
              },
              "example": [
                {
                  "key": "startDate",
                  "value": "2022-01-22 00:00:29",
                  "type": "text"
                },
                {
                  "key": "endDate",
                  "value": "2022-01-22 13:23:29",
                  "type": "text"
                },
                {
                  "key": "externalId",
                  "value": "1341324",
                  "type": "text"
                },
                {
                  "key": "contract_id",
                  "value": "10",
                  "type": "text"
                }
              ]
            }
          }
        },
        "tags": [
          "campaign"
        ]
      }
    },
    "/api/campaigns": {
      "get": {
        "summary": "list",
        "operationId": "list1campaign",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "tags": [
          "campaign"
        ]
      }
    },
    "/api/campaign/{id}": {
      "parameters": [
        {
          "in": "path",
          "name": "id",
          "schema": {
            "type": "integer"
          },
          "required": true
        }
      ],
      "put": {
        "summary": "update",
        "operationId": "update2campaign",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "requestBody": {
          "required": true,
          "content": {
            "application/x-www-form-urlencoded": {
              "schema": {
                "type": "object",
                "required": [
                  "contract_id",
                  "startDate",
                  "endDate",
                  "externalId"
                ],
                "properties": {
                  "contract_id": {
                    "type": "string",
                    "example": "10"
                  },
                  "startDate": {
                    "type": "string",
                    "example": "2022-01-22 00:00:29"
                  },
                  "endDate": {
                    "type": "string",
                    "example": "2022-01-22 00:00:29"
                  },
                  "externalId": {
                    "type": "string",
                    "example": "1"
                  }
                }
              }
            }
          }
        },
        "tags": [
          "campaign"
        ]
      },
      "get": {
        "summary": "view",
        "operationId": "view3campaign",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "tags": [
          "campaign"
        ]
      },
      "delete": {
        "summary": "delete",
        "operationId": "delete4campaign",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "tags": [
          "campaign"
        ]
      }
    },
    "/api/contract": {
      "post": {
        "summary": "create",
        "operationId": "create0contract",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "requestBody": {
          "required": true,
          "content": {
            "multipart/form-data": {
              "schema": {
                "type": "object",
                "required": [
                  "amount",
                  "date",
                  "name",
                  "isVat",
                  "number",
                  "documentationLink",
                  "agency_id",
                  "externalId",
                  "externalContractorId",
                  "externalClientId",
                  "advertiser_id"
                ],
                "properties": {
                  "amount": {
                    "type": "string",
                    "example": "222"
                  },
                  "date": {
                    "type": "string",
                    "example": "2022-01-22 13:23:29"
                  },
                  "name": {
                    "type": "string",
                    "example": "test"
                  },
                  "isVat": {
                    "type": "string",
                    "example": "1"
                  },
                  "number": {
                    "type": "string",
                    "example": "222"
                  },
                  "documentationLink": {
                    "type": "string",
                    "example": "test"
                  },
                  "agency_id": {
                    "type": "string",
                    "example": "7"
                  },
                  "externalId": {
                    "type": "string",
                    "example": "215"
                  },
                  "externalContractorId": {
                    "type": "string",
                    "example": "555"
                  },
                  "externalClientId": {
                    "type": "string",
                    "example": "666"
                  },
                  "advertiser_id": {
                    "type": "string",
                    "example": "4"
                  }
                }
              },
              "example": [
                {
                  "key": "amount",
                  "value": "222",
                  "type": "text"
                },
                {
                  "key": "date",
                  "value": "2022-01-22 13:23:29",
                  "type": "text"
                },
                {
                  "key": "name",
                  "value": "test",
                  "type": "text"
                },
                {
                  "key": "isVat",
                  "value": "1",
                  "type": "text"
                },
                {
                  "key": "number",
                  "value": "222",
                  "type": "text"
                },
                {
                  "key": "documentationLink",
                  "value": "test",
                  "type": "text"
                },
                {
                  "key": "agency_id",
                  "value": "7",
                  "type": "text"
                },
                {
                  "key": "externalId",
                  "value": "215",
                  "type": "text"
                },
                {
                  "key": "externalContractorId",
                  "value": "555",
                  "type": "text"
                },
                {
                  "key": "externalClientId",
                  "value": "666",
                  "type": "text"
                },
                {
                  "key": "advertiser_id",
                  "value": "4",
                  "type": "text"
                }
              ]
            }
          }
        },
        "tags": [
          "contract"
        ]
      }
    },
    "/api/contracts": {
      "get": {
        "summary": "list",
        "operationId": "list1contract",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "tags": [
          "contract"
        ]
      }
    },
    "/api/contract/{id}": {
      "parameters": [
        {
          "in": "path",
          "name": "id",
          "schema": {
            "type": "integer"
          },
          "required": true
        }
      ],
      "put": {
        "summary": "update",
        "operationId": "update2contract",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "requestBody": {
          "required": true,
          "content": {
            "application/x-www-form-urlencoded": {
              "schema": {
                "type": "object",
                "required": [
                  "amount",
                  "date",
                  "name",
                  "isVat",
                  "number",
                  "documentationLink",
                  "agency_id",
                  "externalId",
                  "externalContractorId",
                  "externalClientId"
                ],
                "properties": {
                  "amount": {
                    "type": "string",
                    "example": "222"
                  },
                  "date": {
                    "type": "string",
                    "example": "2022-01-22 13:23:29"
                  },
                  "name": {
                    "type": "string",
                    "example": "test"
                  },
                  "isVat": {
                    "type": "string",
                    "example": "1"
                  },
                  "number": {
                    "type": "string",
                    "example": "222"
                  },
                  "documentationLink": {
                    "type": "string",
                    "example": "test"
                  },
                  "agency_id": {
                    "type": "string",
                    "example": "2"
                  },
                  "externalId": {
                    "type": "string",
                    "example": "4"
                  },
                  "externalContractorId": {
                    "type": "string",
                    "example": "22"
                  },
                  "externalClientId": {
                    "type": "string",
                    "example": "11"
                  }
                }
              }
            }
          }
        },
        "tags": [
          "contract"
        ]
      },
      "get": {
        "summary": "view",
        "operationId": "view3c",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "tags": [
          "contract"
        ]
      },
      "delete": {
        "summary": "delete",
        "operationId": "delete4contract",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "tags": [
          "contract"
        ]
      }
    },
    "/api/creative": {
      "post": {
        "summary": "create",
        "operationId": "create0creative",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "requestBody": {
          "required": true,
          "content": {
            "multipart/form-data": {
              "schema": {
                "type": "object",
                "required": [
                  "category",
                  "type",
                  "url",
                  "mediaUrl",
                  "textData",
                  "targeting_region_id",
                  "campaign_id",
                  "externalId"
                ],
                "properties": {
                  "category": {
                    "type": "string",
                    "example": "1"
                  },
                  "type": {
                    "type": "string",
                    "example": "2"
                  },
                  "url": {
                    "type": "string",
                    "example": "test"
                  },
                  "mediaUrl": {
                    "type": "string",
                    "example": "1"
                  },
                  "textData": {
                    "type": "string",
                    "example": "222"
                  },
                  "targeting_region_id": {
                    "type": "string",
                    "example": "1"
                  },
                  "campaign_id": {
                    "type": "string",
                    "example": "4"
                  },
                  "externalId": {
                    "type": "string",
                    "example": "23"
                  }
                }
              },
              "example": [
                {
                  "key": "category",
                  "value": "1",
                  "type": "text"
                },
                {
                  "key": "type",
                  "value": "2",
                  "type": "text"
                },
                {
                  "key": "url",
                  "value": "test",
                  "type": "text"
                },
                {
                  "key": "mediaUrl",
                  "value": "1",
                  "type": "text"
                },
                {
                  "key": "textData",
                  "value": "222",
                  "type": "text"
                },
                {
                  "key": "targeting_region_id",
                  "value": "1",
                  "type": "text"
                },
                {
                  "key": "campaign_id",
                  "value": "4",
                  "type": "text"
                },
                {
                  "key": "externalId",
                  "value": "23",
                  "type": "text"
                }
              ]
            }
          }
        },
        "tags": [
          "creative"
        ]
      }
    },
    "/api/creatives": {
      "get": {
        "summary": "list",
        "operationId": "list1creative",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "tags": [
          "creative"
        ]
      }
    },
    "/api/creative/{id}": {
      "parameters": [
        {
          "in": "path",
          "name": "id",
          "schema": {
            "type": "integer"
          },
          "required": true
        }
      ],
      "put": {
        "summary": "update",
        "operationId": "update2creative",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "requestBody": {
          "required": true,
          "content": {
            "application/x-www-form-urlencoded": {
              "schema": {
                "type": "object",
                "required": [
                  "category",
                  "type",
                  "url",
                  "mediaUrl",
                  "textData",
                  "targeting_region_id",
                  "campaign_id"
                ],
                "properties": {
                  "category": {
                    "type": "string",
                    "example": "1"
                  },
                  "type": {
                    "type": "string",
                    "example": "2"
                  },
                  "url": {
                    "type": "string",
                    "example": "test"
                  },
                  "mediaUrl": {
                    "type": "string",
                    "example": "1"
                  },
                  "textData": {
                    "type": "string",
                    "example": "222"
                  },
                  "targeting_region_id": {
                    "type": "string",
                    "example": "1"
                  },
                  "campaign_id": {
                    "type": "string",
                    "example": "6"
                  }
                }
              }
            }
          }
        },
        "tags": [
          "creative"
        ]
      },
      "get": {
        "summary": "view",
        "operationId": "view3creative",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "tags": [
          "creative"
        ]
      },
      "delete": {
        "summary": "delete",
        "operationId": "delete4creative",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "tags": [
          "creative"
        ]
      }
    },
    "/api/invoice": {
      "post": {
        "summary": "create",
        "operationId": "create0invoice",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "requestBody": {
          "required": true,
          "content": {
            "multipart/form-data": {
              "schema": {
                "type": "object",
                "required": [
                  "amount",
                  "documentLink",
                  "endDate",
                  "isVat",
                  "startDate",
                  "externalId",
                  "campaign_id",
                  "contract_id"
                ],
                "properties": {
                  "amount": {
                    "type": "string",
                    "example": "1"
                  },
                  "documentLink": {
                    "type": "string",
                    "example": "2"
                  },
                  "endDate": {
                    "type": "string",
                    "example": "2022-01-22 13:23:29"
                  },
                  "isVat": {
                    "type": "string",
                    "example": "1"
                  },
                  "startDate": {
                    "type": "string",
                    "example": "2022-01-22 13:23:29"
                  },
                  "externalId": {
                    "type": "string",
                    "example": "56"
                  },
                  "campaign_id": {
                    "type": "string",
                    "example": "4"
                  },
                  "contract_id": {
                    "type": "string",
                    "example": "16"
                  }
                }
              },
              "example": [
                {
                  "key": "amount",
                  "value": "1",
                  "type": "text"
                },
                {
                  "key": "documentLink",
                  "value": "2",
                  "type": "text"
                },
                {
                  "key": "endDate",
                  "value": "2022-01-22 13:23:29",
                  "type": "text"
                },
                {
                  "key": "isVat",
                  "value": "1",
                  "type": "text"
                },
                {
                  "key": "startDate",
                  "value": "2022-01-22 13:23:29",
                  "type": "text"
                },
                {
                  "key": "externalId",
                  "value": "56",
                  "type": "text"
                },
                {
                  "key": "campaign_id",
                  "value": "4",
                  "type": "text"
                },
                {
                  "key": "contract_id",
                  "value": "16",
                  "type": "text"
                }
              ]
            }
          }
        },
        "tags": [
          "invoice"
        ]
      }
    },
    "/api/invoices": {
      "get": {
        "summary": "list",
        "operationId": "list1invoice",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "tags": [
          "invoice"
        ]
      }
    },
    "/api/invoice/{id}": {
      "parameters": [
        {
          "in": "path",
          "name": "id",
          "schema": {
            "type": "integer"
          },
          "required": true
        }
      ],
      "put": {
        "summary": "update",
        "operationId": "update2invoice",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "requestBody": {
          "required": true,
          "content": {
            "application/x-www-form-urlencoded": {
              "schema": {
                "type": "object",
                "required": [
                  "amount",
                  "documentLink",
                  "endDate",
                  "isVat",
                  "startDate",
                  "externalId",
                  "campaign_id",
                  "contract_id"
                ],
                "properties": {
                  "amount": {
                    "type": "string",
                    "example": "1"
                  },
                  "documentLink": {
                    "type": "string",
                    "example": "2"
                  },
                  "endDate": {
                    "type": "string",
                    "example": "2022-01-22 13:23:29"
                  },
                  "isVat": {
                    "type": "string",
                    "example": "1"
                  },
                  "startDate": {
                    "type": "string",
                    "example": "2022-01-22 13:23:29"
                  },
                  "externalId": {
                    "type": "string",
                    "example": "1"
                  },
                  "campaign_id": {
                    "type": "string",
                    "example": "1"
                  },
                  "contract_id": {
                    "type": "string",
                    "example": "1"
                  }
                }
              }
            }
          }
        },
        "tags": [
          "invoice"
        ]
      },
      "get": {
        "summary": "view",
        "operationId": "view3invoice",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "tags": [
          "invoice"
        ]
      },
      "delete": {
        "summary": "delete",
        "operationId": "delete4invoice",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "tags": [
          "invoice"
        ]
      }
    },
    "/api/auth": {
      "post": {
        "summary": "auth",
        "operationId": "auth",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "headers": {}
          }
        },
        "requestBody": {
          "required": true,
          "content": {
            "multipart/form-data": {
              "schema": {
                "type": "object",
                "required": [
                  "name",
                  "password"
                ],
                "properties": {
                  "name": {
                    "type": "string",
                    "example": "aggregator"
                  },
                  "password": {
                    "type": "string",
                    "example": "aggregator"
                  }
                }
              },
              "example": [
                {
                  "key": "name",
                  "value": "aggregator",
                  "type": "text"
                },
                {
                  "key": "password",
                  "value": "aggregator",
                  "type": "text"
                }
              ]
            }
          }
        },
        "tags": [
          "auth"
        ]
      }
    }
  },
  "components": {
    "securitySchemes": {
      "bearer": {
        "type": "http",
        "scheme": "bearer"
      }
    }
  },
  "tags": [],
  "externalDocs": {
    "url": "",
    "description": ""
  }
}