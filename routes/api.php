<?php

use App\Http\Controllers\AdvertiserController;
use App\Http\Controllers\AgencyController;
use App\Http\Controllers\CampaignController;
use App\Http\Controllers\ContractController;
use App\Http\Controllers\CreativeController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\StatsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\Passport;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/auth', function (Request $request) {
    $credentials = $request->only('name', 'password');
    if ($result = Auth::guard('web')->attempt($credentials, false, false)) {
        Passport::personalAccessTokensExpireIn(now()->addSeconds(6400000));
        $token = Auth::user()->createToken('token')->accessToken;
        return response()->json([
            'token' => $token
        ]);
    }
    return response()->json([
        'status' => '403',
        'data' => 'User with provided username and password not found'
    ]);
});
Route::middleware('auth:api')->group(function () {
    Route::get('/user', function (Request $request) {
        return response()->json([
            'status' => 'OK',
            'data' => $request->user()->getAttributes()
        ]);
    });
    Route::get('/advertisers', [AdvertiserController::class, 'list']);
    Route::post('/advertiser', [AdvertiserController::class, 'create']);
    Route::put('/advertiser/{id}', [AdvertiserController::class, 'update']);
    Route::delete('/advertiser/{id}', [AdvertiserController::class, 'delete']);
    Route::get('/advertiser/{id}', [AdvertiserController::class, 'view']);
    Route::get('/advertiser/{external_id}', [AdvertiserController::class, 'external']);

    Route::get('/agencies', [AgencyController::class, 'list'])->name('get_agencies');
    Route::post('/agency', [AgencyController::class, 'create']);
    Route::put('/agency/{id}', [AgencyController::class, 'update']);
    Route::delete('/agency/{id}', [AgencyController::class, 'delete']);
    Route::get('/agency/{id}', [AgencyController::class, 'view']);
    Route::get('/agency/{external_id}', [AgencyController::class, 'external']);

    Route::get('/campaigns', [CampaignController::class, 'list']);
    Route::post('/campaign', [CampaignController::class, 'create']);
    Route::put('/campaign/{id}', [CampaignController::class, 'update']);
    Route::delete('/campaign/{id}', [CampaignController::class, 'delete']);
    Route::get('/campaign/{id}', [CampaignController::class, 'view']);
    Route::get('/campaign/{external_id}', [CampaignController::class, 'external']);

    Route::get('/stats', [StatsController::class, 'list']);
    Route::post('/stat', [StatsController::class, 'create']);
    Route::put('/stat/{id}', [StatsController::class, 'update']);
    Route::delete('/stat/{id}', [StatsController::class, 'delete']);
    Route::get('/stat/{id}', [StatsController::class, 'view']);

    Route::get('/creatives', [CreativeController::class, 'list']);
    Route::post('/creative', [CreativeController::class, 'create']);
    Route::put('/creative/{id}', [CreativeController::class, 'update']);
    Route::delete('/creative/{id}', [CreativeController::class, 'delete']);
    Route::get('/creative/{id}', [CreativeController::class, 'view']);
    Route::get('/creative/{external_id}', [CreativeController::class, 'external']);

    Route::get('/contracts', [ContractController::class, 'list']);
    Route::post('/contract', [ContractController::class, 'create']);
    Route::put('/contract/{id}', [ContractController::class, 'update']);
    Route::delete('/contract/{id}', [ContractController::class, 'delete']);
    Route::get('/contract/{id}', [ContractController::class, 'view']);
    Route::get('/contract/{external_id}', [ContractController::class, 'external']);

    Route::get('/invoices', [InvoiceController::class, 'list']);
    Route::post('/invoice', [InvoiceController::class, 'create']);
    Route::put('/invoice/{id}', [InvoiceController::class, 'update']);
    Route::delete('/invoice/{id}', [InvoiceController::class, 'delete']);
    Route::get('/invoice/{id}', [InvoiceController::class, 'view']);
    Route::get('/invoice/{external_id}', [InvoiceController::class, 'external']);
});
