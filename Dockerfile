FROM php:8.0.14-fpm

# Arguments defined in docker-compose.yml
ARG user
ARG uid

# Install system dependencies
RUN apt-get update && apt-get install -y \
    git \
    curl \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    zip \
    unzip
RUN pecl install xdebug \
    && docker-php-ext-enable xdebug

RUN mkdir -p docker/php/conf.d \
    && touch docker/php/conf.d/xdebug.ini \
    && touch docker/php/conf.d/error_reporting.ini
# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install PHP extensions
RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Create system user to run Composer and Artisan Commands
RUN useradd -G www-data,root -u $uid -d /home/$user $user
RUN mkdir -p /home/$user/.composer && \
    chown -R $user:$user /home/$user
# Set working directory
WORKDIR /var/www
RUN cd /var/www

USER $user
ENTRYPOINT [ "/var/www/entrypoint.sh" ]
CMD ["php-fpm"]
