##Импорт данных в API ЕРИР

###Настройки

Файл `config/erir.php` с настройками подключения клиента.

##### Провайдеры и маппинг данных (при использовании табличных источников)

Провайдер по умолчанию указывается в параметре (`'default_import'`)

Настройка (`'import'`). Любые специфичные для провайдера параметры указываются в массиве с названием этого провайдера. Настройка `'mapping'` для указания соответствия столбцов данным импортируемого объекта.  

###Пример работы импорта

####Через обращение к фасаду Import
```php
use App\Services\Erir\Facades\Import;

Import::agencies([
    'from' => $filePath
]);

//Для выбора провайдера файлов не по умолчанию
Import::provider('xlsx')->agencies([
    'from' => $filePath
]);
```

#### Или использовать DI вместо фасада

```php
use App\Services\Erir\Import\ErirImport;

class ImportCsvAgency
{
    public function __construct(
        protected ErirImport $import,
    ){}

    public function handle(string $filePath): void
    {
        $this->import
            ->agencies([
                'from' => $filePath
            ]);
    }
}
```

#### Встроенная команда импорта из csv файла 

Поместить импортируемый файл в директорию выполнения команды и запустить импорт.
```bash
php artisan erir:import-csv --file=agencies.csv --entity=agencies
```


