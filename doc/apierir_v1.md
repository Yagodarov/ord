# API ЕРИР PHP Client

## Подключение

В список провайдеров `config/app.php` добавить сервис провайдер 
```php
    App\Providers\ErirServiceProvider::class
````

Файл `config/erir.php` с настройками подключения клиента. JWT токен авторизаций ERIR_TOKEN нужно указать в .env


##### Адрес подключения к сервису

Настройка (`'default'`) для определения используемого подключения, по умолчанию `'main'`. Может быть использовано несколько подключений (смена данных авторизации без смены контекста).

##### Настройки подключения

Настройка (`'connections'`). Конфигурация типа авторизации: `non`, `oauth` или `token`, используемые данные для авторизации в соответствии с типом (`token`, `client_*`). Версии используемого подключением API, по умолчанию `1`.

##### Включение кеширования

Настройка (`'cache'`) для включения/отключения кеширования, определение драйвера работы с кешем.

##### Включение логирования

Параметр (`'log'`) включение/отключение, определение драйвера логирования.


### Структура клиента

- Auth: объекты для работы с авторизацией
- Api: контролеры эндпоинтов EРИР и базовый абстрактный API
- DTO: конвертация модели объекта в формат ЕРИР  
- Facades: фасад ацессора регистрации и для алиаса
- Exceptions: кастомные ошибки, наследуются от HTTP Exeptions
- HttpClient: клиент билдера клиента подключения, кеширования, запросов и ответов, валидации
- Client: клиент фабрики методов API и конструктор подключения к HttpClient

Для уменьшения связности с сообщениями HTTP клиента используются [PSR-7](https://www.php-fig.org/psr/psr-7/), [PSR-17](https://www.php-fig.org/psr/psr-17/), [PSR-18](https://www.php-fig.org/psr/psr-18/), and [HTTPlug](https://httplug.io/). 

## Примеры работы с API

##### ErirManager

Основной класс для работы с API, через создание из IOC контейнера  `'gitlab'` может быть использован через фасад `Facades\Erir`. Менеджер ЕРИР реализует `ManagerInterface` и расширяет `AbstractManager`, метод подключения `connection()` возвращает объект класса `Erir\Client`.

Так как настройки подключения указаны в файле конфигурации, и подключение по умолчанию так же указано (`main`), есть возможность сразу же обращаться к фасаду и получать/отправлять данные:

```php
use App\Services\Erir\Facades\Erir;
// может быть использовано без директивы, если создан алиас в config/app.php

Erir::campaign()->getById($campaingId);
```

Для использования определённого подключения, название из конфигурации передается в `connection()`:

```php
use App\Services\Erir\Facades\Erir;

// подключение по умолчанию, можно не указывать:
Erir::connection('main')->invoices()->getById($invoiceId);

// тоже самое что и:
Erir::invoices()->getById($invoiceId);

// установка подключения
Erir::setDefaultConnection('test_auth'); 
```

Вместо фасада можно использовать DI:

```php
use App\Services\Erir\ErirManager;
use App\Models\Agency;

class SyncErirAgency
{
    public function __construct(
        protected ErirManager $erir,
    ){}

    public function handle(Agency $agency): void
    {
        $this->erir->agencies()->create(
            id: $agency->id,
            inn: $agency->inn,
            name: $agency->name
        );
    }
}

```

### Обработка запроса пользователя
![Banner](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuL8ioKZDJLKeo4dCpECg00fc9cSM9EQLA5oOVs7VXTqNUovi1zARBcous7HXAuZFXKj2GTTEuSAy2mikJB5I24ejB4qjBk5AJ2x9B4iiIOLAS44eP9N5YMMf2gbv9PabbGZLle48bK2EbK9oKIf8F2i_YSitK44NINz6ObvA3aGc9HMx0HUc0bapBYomKd0CyCHm6boQ2yP0CHy8eDgA2_BpYbCHjY1qGkq2wSCPR5QO9Y22nI4rBmKO6G00)